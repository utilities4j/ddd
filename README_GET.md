# GET

## Endpoints

### 1. Buscar Entidades com Paginação

**GET /domain/{entityName}**

Busca uma lista paginada de entidades.

#### Parâmetros
- `entityName` (path): Nome da entidade.
- `fields` (query, opcional): Lista de campos a serem retornados. Exemplo: `fields=id,name,relatedEntity.name`
- `filter` (query, opcional): Filtro para aplicação na consulta.
- `page` (query, opcional): Número da página (padrão: 0).
- `size` (query, opcional): Tamanho da página (padrão: 10).
- `sort` (query, opcional): Ordenação dos resultados. Exemplo: `name,asc`

#### Exemplo de Uso

```bash
GET /domain/Escola?fields=id,name,diretor.name&filter=status=1&page=0&size=10&sort=name,asc
```

#### Resposta

```json
{
  "content": [
    {
      "id": 1,
      "name": "Escola A",
      "diretor": {
        "name": "Diretor A"
      }
    }
  ],
  "totalPages": 5,
  "totalElements": 50
}
```

### 2. Buscar Entidade por ID

**GET /domain/{entityName}/{id}**

Busca uma entidade pelo seu ID.

#### Parâmetros
- `entityName` (path): Nome da entidade.
- `id` (path): ID da entidade.
- `fields` (query, opcional): Lista de campos a serem retornados. Exemplo: `fields=id,name,relatedEntity.name`

#### Exemplo de Uso

```bash
GET /domain/Escola/1?fields=id,name,diretor.name
```

#### Resposta

```json
{
  "id": 1,
  "name": "Escola A",
  "diretor": {
    "name": "Diretor A"
  }
}
```

### 3. Buscar Todas as Entidades

**GET /domain/{entityName}/all**

Busca todas as entidades.

#### Parâmetros
- `entityName` (path): Nome da entidade.
- `fields` (query, opcional): Lista de campos a serem retornados. Exemplo: `fields=id,name,relatedEntity.name`
- `filter` (query, opcional): Filtro para aplicação na consulta.
- `sort` (query, opcional): Ordenação dos resultados. Exemplo: `name,asc`

#### Exemplo de Uso

```bash
GET /domain/Escola/all?fields=id,name,diretor.name
```

#### Resposta 

```json
[
  {
    "id": 1,
    "name": "Escola A",
    "diretor": {
      "name": "Diretor A"
    }
  },
  {
    "id": 2,
    "name": "Escola B",
    "diretor": {
      "name": "Diretor B"
    }
  }
]
```

### 5. Buscar Entidades com Filtros, Ordenação e Paginação

**GET /domain/{entityName}**

Busca uma lista paginada de entidades com filtros e ordenação.

#### Parâmetros
- `entityName` (path): Nome da entidade.
- `fields` (query, opcional): Lista de campos a serem retornados. Exemplo: `fields=id,name,relatedEntity.name`
- `filter` (query, opcional): Filtro para aplicação na consulta. Exemplo: `filter=name:like:Escola`
- `page` (query, opcional): Número da página (padrão: 0).
- `size` (query, opcional): Tamanho da página (padrão: 10).
- `sort` (query, opcional): Ordenação dos resultados. Exemplo: `sort=name,asc`

#### Exemplo de Uso

```bash
GET /domain/Escola?fields=id,name,diretor.name&filter=status=1&sort=name,asc&page=0&size=10
```

#### Resposta 

```json
{
  "content": [
    {
      "id": 1,
      "name": "Escola A",
      "diretor": {
        "name": "Diretor A"
      }
    }
  ],
  "totalPages": 5,
  "totalElements": 50
}
```

## Contribuição

Contribuições são bem-vindas! Por favor, abra uma issue ou envie um pull request para melhorias e correções.

Se quiser contribuir com o projeto, por favor siga os passos abaixo:

1. Faça um fork do projeto.
2. Crie uma branch para a sua feature (git checkout -b feature/nova-feature).
3. Commit suas mudanças (git commit -am 'Adiciona nova feature').
4. Faça push para a branch (git push origin feature/nova-feature).
5. Crie um novo Pull Request.

## Licença
Este projeto está licenciado sob a licença MIT - veja o arquivo LICENSE para mais detalhes.


# REST Query Language

## Introdução
A REST Query Language permite realizar operações avançadas de filtragem, ordenação, paginação e seleção de campos em recursos expostos pela API. A sintaxe e os operadores suportados são descritos abaixo.

## Sintaxe de Consulta

### Estrutura Geral

A URL de uma consulta pode conter os seguintes parâmetros:

- **filter**: Define critérios de filtragem.
- **sort**: Define a ordenação dos resultados.
- **fields**: Define os campos a serem retornados.
- **page**: Define o número da página de resultados.
- **size**: Define o número de itens por página.

Exemplo:
```
GET /domain/products?filter=(name~phone,category:electronics+price>500)&sort=price,asc&fields=id,name,price&page=1&size=20
```

## Filtragem

### Parâmetro `filter`

O parâmetro `filter` permite especificar condições de filtragem usando a seguinte expressão regular:

### Componentes da Expressão

- **Campo**: O nome do campo a ser filtrado. Campos aninhados são suportados usando ponto (.)
- **Operador**: Define o tipo de operação:
  - ` eq ` igual
  - ` ne ` diferente
  - ` gt ` maior que
  - ` ge ` maior ou igual 
  - ` lt ` menor que
  - ` le ` menor ou igual 
  - ` like ` LIKE (contém)
- **Valor**: O valor a ser comparado.
- **Separador**: Define a combinação de condições:
  - ` and ` AND
  - ` or ` OR

### Exemplos de Filtragem

1. **Igualdade**
   - Consulta: `domain/pedido?filter=cliente.nome eq 'John Wick'`
   - Descrição: Retorna pedidos dos clientes de nome Jonh Wick".

2. **Diferente**
   - Consulta: `filter=category!appliances`
   - Descrição: Retorna produtos que não estão na categoria "appliances".

3. **Maior que**
   - Consulta: `domain/produto?filter=price gt 500`
   - Descrição: Retorna produtos com preço maior que 500.

4. **Menor que**
   - Consulta: `domain/produto?filter=price le 1000`
   - Descrição: Retorna produtos com preço menor/igual a 1000.

5. **LIKE (Contém)**
   - Consulta: `domain/produto?filter=name like phone`
   - Descrição: Retorna produtos cujo nome contém "phone".

6. **Combinação AND**
   - Consulta: `domain/produto?filter=price ge 500 and price le 1000`
   - Descrição: Retorna produtos com preço maior/igual a 500 e menor/igual a 1000.

7. **Combinação OR**
   - Consulta: `domain/produto?filter=nome like 'phone' or preco gt 500`
   - Descrição: Retorna produtos que contem 'phone' no nome ou preço maior que 500.

8. **Campos Aninhados**
   - Consulta: `filter=manufacturer.name~Sony`
   - Descrição: Retorna produtos cujo nome do fabricante contém "Sony".

9. **Combinação com Campos Aninhados**
   - Consulta: `filter=manufacturer.name~Sony+manufacturer.country:Japan`
   - Descrição: Retorna produtos cujo nome do fabricante contém "Sony" e o país do fabricante é "Japan".

## Ordenação

### Parâmetro `sort`

O parâmetro `sort` define a ordenação dos resultados. A sintaxe é:

```
sort=campo,ordem
```

- **campo**: O campo pelo qual os resultados devem ser ordenados. Campos aninhados são suportados.
- **ordem**: A direção da ordenação (`asc` para ascendente, `desc` para descendente).

Exemplo:
```
sort=price,asc
```
Descrição: Ordena os produtos pelo campo "price" em ordem crescente.

## Seleção de Campos

### Parâmetro `fields`

O parâmetro `fields` especifica quais campos devem ser retornados na resposta. A sintaxe é:

```
fields=campo1,campo2,...
```

Campos aninhados são suportados.

Exemplo:
```
fields=id,name,price,manufacturer.name
```
Descrição: Retorna apenas os campos "id", "name", "price" e "manufacturer.name" dos produtos.

## Paginação

### Parâmetros `page` e `size`

Os parâmetros `page` e `size` definem a paginação dos resultados.

- **page**: O número da página (inicia em 0).
- **size**: O número de itens por página.

Exemplo:
```
page=1&size=20
```
Descrição: Retorna a segunda página de resultados (página 1) com 20 itens por página.

## Exemplo Completo

Um exemplo de consulta completa utilizando todos os parâmetros:

```
GET /products?filter=(name~phone,category:electronics+price>500)&sort=price,asc&fields=id,name,price,manufacturer.name&page=1&size=20
```

- **Filtragem**: Nome contendo "phone" OU categoria "electronics" E preço maior que 500.
- **Ordenação**: Ordena por preço em ordem crescente.
- **Seleção de Campos**: Retorna apenas os campos "id", "name", "price" e "manufacturer.name".
- **Paginação**: Retorna a segunda página com 20 itens por página.

## Conclusão

Esta REST Query Language proporciona uma interface poderosa e flexível para realizar consultas complexas e precisas nos dados da sua API. Adapte e expanda conforme necessário para atender aos requisitos específicos do seu aplicativo.
