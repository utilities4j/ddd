# PATCH

## Endpoints
  Permitem adicionar e remover itens de coleções dentro de entidades:

- `PATCH /domain/{entityName}/{entityId}/{collectionFieldName}/add`
- `PATCH /domain/{entityName}/{entityId}/{collectionFieldName}/add/{itemId}`
- `PATCH /domain/{entityName}/{entityId}/{collectionFieldName}/remove/{itemId}`

Em java seria algo equivalente a:
```Java
  Pedido pedido = entityManager.find(Pedido.class, entityId);
  Item item = entityManager.find(Item.class, itemId);
  pedido.getItens().add(item); // ou pedido.getItens().remove(item);
  entityManager.merge(pedido);
```

### Adicionando um Item à uma Coleção de **COMPOSIÇÃO**

#### `PATCH /domain/{entityName}/{entityId}/{collectionFieldName}/add`

Adiciona um item à coleção especificada na entidade pelo objeto.

**Parâmetros**

- `entityName` : O nome da entidade (singular e em kebab case)
- `entityId` : O ID da entidade.
- `collectionFieldName` : O nome do campo de coleção na entidade (por exemplo, `itens`).
- **Corpo da Requisição** : um JSON representando o objeto a ser adicionado à coleção. 

**Resposta**

- `204 No Content`: Indica que o item foi adicionado a lista com sucesso.
- `404 Not Found`: Se a entidade ou a coleção não forem encontradas.

**Exemplo de Requisição**

Adiciona um item ao pedido, e no caso, deve incluir a referência ao `Produto`.

```bash
curl -X PATCH "http://localhost:8080/domain/pedido/1/itens/add" \
-H "Content-Type: application/json" \
-d '{
     "quantidade": 2,
     "valor": 50,
     "produto": { "id": 1 }
    }'
```

### Adicionando um Item à uma Coleção de **AGREGAÇÃO**

#### `PATCH /domain/{entityName}/{entityId}/{collectionFieldName}/add/{itemId}`

Adiciona um item à coleção especificada na entidade pelo ID do item.

**Parâmetros**

- `entityName` : O nome da entidade (singular e em kebab case)
- `entityId` : O ID da entidade.
- `collectionFieldName` : O nome do campo de coleção na entidade (por exemplo, `itens`).
- `itemId` : O ID do item a ser adicionado/associado a entidade.

**Resposta**

- `204 No Content`: Indica que o item foi adicionado/associado a entidade  com sucesso.
- `404 Not Found`: Se o pedido, a coleção de vendedores ou o vendedor não forem encontrados.

**Exemplo de Requisição**
Adicionando um vendedor a um pedido específico usando o ID do vendedor.

```bash
curl -X PATCH "http://localhost:8080/domain/pedido/1/vendedores/add/33"
```

### Removendo um Item da Coleção

#### `PATCH /domain/{entityName}/{entityId}/{collectionFieldName}/remove/{itemId}`

Remove um item da coleção especificada na entidade pelo ID do item.

**Parâmetros**

- `entityName` : O nome da entidade (singular e em kebab case).
- `entityId` : O ID da entidade.
- `collectionFieldName` : O nome do campo de coleção na entidade (por exemplo, `addresses`).
- `itemId` : O ID do item a ser removido da coleção.

**Resposta**

- `204 No Content`: Indica que o item foi removido da lista com sucesso.
- `404 Not Found`: Se a entidade, a coleção ou o item não forem encontrados.

**Exemplo de Requisição**

```bash
curl -X PATCH "http://localhost:8080/domain/pedido/1/itens/remove/1"
```



# Licença

Este projeto está licenciado sob os termos da licença MIT. Consulte o arquivo `LICENSE` para obter mais informações.
