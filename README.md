# SPRING DDD-NOAP

## Introdução
- Spring com o poder do Domain-Driven Design (DDD) e do Naked Object Architectural Pattern (NOAP).
  Reduz a quantidade de código boilerplate (código repetitivo e de baixo valor) 
  que você precisa escrever em seus @Repository e @Controller.
  - O DDDRepository permite utilizar apenas um único @Repository para todas as entidades do projeto Spring.
  - A Naked Object RESTfull API permite operações avançadas de CRUD (Create, Read, Update, Delete) em entidades

## Índice
1. [Introdução](#introdução)
2. [Pré-requisitos](#pré-requisitos)
3. [Configuração do Projeto](#configuração-do-projeto)
4. [Naked Object Restful Dynamic API](#naked-object-restful-dynamic-api)
    - [POST Endpoints](#post-endpoints)
    - [PATCH Endpoints](./README_PATCH.md)
    - [DELETE Endpoints](#delete-endpoints)
    - [GET Endpoints](./README_GET.md)
    - [RQL - Rest Query Language](#rql)
       - [Filtragem e Ordenação](#filtragem-e-ordenação)
5. [DDD](#DDD)
6. [Contribuição](#contribuição)
7. [Licença](#licença)

## Pré-requisitos
- [Java 11 ou superior](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
- [Maven 3.6.3 ou superior](https://maven.apache.org/download.cgi)

## Configuração do Projeto
**1. No pom.xml:**
```xml
</dependencies>
    ...
    <dependency>
      <groupId>javax.util</groupId>
      <artifactId>ddd-noap</artifactId>
      <version>1.0.0</version>
      <type>jar</type>
    </dependency>
    ...
</dependencies>
<repositories>
    ...
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/groups/13274432/-/packages/maven</url>
    </repository>
    ...
</repositories>
```
  
**2. No bean de configuração do Spring**
```java
@Configuration
@ComponentScan(basePackages = {"javax.util.spring.noap.restfull"})
public class Config {
}
```

## Naked Object Restfull Dynamic API
   Esta API permite buscar entidades dinâmicamente com campos personalizados, filtragem, ordenação e paginação.

### Operações com as Entidades

| Operação  | Verbo  | Body Request          | Status Sucesso  |Ex.Body Response|
|:----------|:-------|-----------------------|:---------------:|:--------------:|
| Persistir | POST   | Entidade json formato | 201 Created     |{ "id"=99 }     |
| Alterar   | PATCH  | Entidade json formato | 204 Not Content |      --        |
| Remover   | DELETE |                       | 204 Not Content |      --        |
| Consultar | GET    |                       | 200 Sucess      |                |

- Todos os endpoints possuem O PATH base:
  - `VERBO /domain/{entityName}`
  - Onde `entityName` é nome da entidade no singular e em [Kebab-case](https://coodesh.com/blog/candidates/dicas/convencoes-de-codificacao-do-camelcase-ao-kebab-case/).
    - <details><summary>Menor Gap Semântico</summary>Distância entre o problema no mundo real e o modelo abstrato construído. Quanto menor esse gap, mais direto é o mapeamento e, portanto, mais rapidamente serão construídas soluções para o problema.</details>
  - Exemplos: 
    - `POST /domain/edital`  (Classe Edital)
    - `GET /domain/nota-fiscal/all` (Classe NotaFiscal)

**Observação:**
Nos exemplos seguintes, utilizamos as entidades `Pedido`, `Item`, `Produto` e `Vendedor`: [Domain Model Example](./README_DOMAIN_MODEL.md)

### POST Endpoints
  Persiste ou instancia uma entidade.
  - `POST /domain/{entityName}/new`
  - `POST /domain/{entityName}[?fields=field1,...,fieldN]`
    - Se **fields** for informado, os campos listados serão retornados no json response.
  - [Exemplos de uso](./README_POST.md)

### DELETE Endpoints
  Remove uma entidade.
  - `DELETE /domain/{entityName}/{id}`
  - Exemplo de uso: 
    - `DELETE /domain/pedido/33`

- **Query Parâmetros**
  - `fields` (opcional): Lista de campos a serem retornados. 
    - Exemplos: 
    - `GET /pedido/all?fields=id,data,cliente.id,cliente.nome,valorTotal`

### Códigos de Resposta 4xx
- `400 Bad Request`: A requisição não pôde ser entendida pelo servidor devido a sintaxe malformada.
- `404 Not Found`: A entidade/id não foi encontrada.

## Licença

