# GlobalExceptionHandler

## Introdução
Este documento descreve brevemente a utilização das exception 

## jakarta.persistence.EntityNotFoundException

- **Contexto**: Persistência de dados (JPA)
- **Uso**: Lançada quando uma entidade não é encontrada no banco de dados.
- **Resposta de Erro**:
- Status HTTP: `404 Not Found`
- Mensagem: `"Classe da entidade '{entityName}' não encontrada"`


### Exemplo

```java
try {
    MyEntity entity = entityManager.getReference(MyEntity.class, id);
} catch (EntityNotFoundException e) {
    throw new EntityNotFoundException("MyEntity (" + id + ")");
}
```

## java.util.NoSuchElementException

- **Contexto**: Coleções e iteradores
- **Uso**: Lançada ao tentar acessar um elemento ausente.
- **Resposta de Erro**:
- Status HTTP: `404 Not Found`
- Mensagem: `"Classe da entidade '{entityName}' não encontrada"`

### Exemplo

```java
try {
    String element = iterator.next();
} catch (NoSuchElementException e) {
    // Sem mais elementos
}
```

## java.lang.ClassNotFoundException

- **Contexto**: Coleções e iteradores
- **Uso**: Lançada quando uma classe não pode ser encontrada durante operações de reflexão ou carregamento dinâmico.
- **Resposta de Erro**:
- Status HTTP: `404 Not Found`
- Mensagem: `"Classe da entidade '{entityName}' não encontrada"`

**Exemplo:**

```java
try {
    Class<?> clazz = Class.forName("com.example.MyClass");
} catch (ClassNotFoundException e) {
    throw new ClassNotFoundException(entityName);
}
```
