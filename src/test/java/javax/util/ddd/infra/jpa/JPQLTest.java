package javax.util.ddd.infra.jpa;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class JPQLTest {

    String[] aJpql = new String[]{
        "FROM Pessoa",
        "FROM Pessoa p",
        "FROM Pessoa as p",
        "FROM Pessoa WHERE idade >= 18",
        "FROM Pessoa p WHERE p.idade >= 18",
        "FROM Pessoa as p WHERE p.idade >= 18",
        "FROM Pessoa ORDER BY idade >= 18",
        "FROM Pessoa p ORDER BY idade >= 18",
        "FROM Pessoa as p ORDER BY idade >= 18",
        "FROM Pessoa WHERE idade >= 18 ORDER BY idade >= 18",
        "FROM Pessoa p WHERE idade >= 18 ORDER BY idade >= 18",
        "FROM Pessoa as p WHERE idade >= 18 ORDER BY idade >= 18", //"SELECT p FROM Pessoa p",
    //"SELECT p FROM Pessoa as p",
    //"SELECT p FROM Pessoa p WHERE p.idade >= 18", //retornando "p"
    //"SELECT p FROM Pessoa as p WHERE p.idade >= 18",//retornando "p"
    //"SELECT p FROM Pessoa p ORDER BY idade >= 18",//retornando "p"
    //"SELECT p FROM Pessoa as p ORDER BY idade >= 18",//retornando "p"
    //"SELECT p FROM Pessoa p WHERE idade >= 18 ORDER BY idade >= 18",//retornando "p"
    //"SELECT p FROM Pessoa as p WHERE idade >= 18 ORDER BY idade >= 18"//retornando "p"
    };

    @Test
    void testextrairPropriedadesEAliasDoSelect1() {
        for (String jpql : aJpql) {
            JPQL analyzer = new JPQL(jpql);
            List<String> expected = Arrays.asList();
            List<String> result = analyzer.extrairPropriedadesEAliasDoSelect();
            assertEquals(expected, result, "divergencia na jpql : " + jpql);
        }
    }

    @Test
    void testextrairPropriedadesEAliasDoSelect3() {
        String select = "SELECT p.nome, idade, p.endereco.cidade.nome, contato.email as contatoEmail, contato.fone";

        for (String jpql : aJpql) {
            JPQL analyzer = new JPQL(select + " " + jpql);
            List<String> expected = Arrays.asList("p.nome", "idade", "p.endereco.cidade.nome", "contatoEmail", "contato.fone");
            List<String> result = analyzer.extrairPropriedadesEAliasDoSelect();
            assertEquals(expected, result, "divergencia na jpql : " + jpql);
        }
    }

//    @Test
//    void testextrairPropriedadesEAliasDoSelect4() {
//        String jpql = "SELECT count(*) FROM Pessoa p  WHERE idade >= 18";
//        List<String> expected = Arrays.asList("count");
//        List<String> result = extrairPropriedadesEAliasDoSelect(jpql);
//        assertEquals(expected, result, "Os parâmetros extraídos devem corresponder aos esperados");
//    }
    @Test
    void testextrairPropriedadesEAliasDoSelect5() {
        String select = "SELECT nome, idade, endereco.cidade.nome, contato.email as contatoEmail, contato.fone FROM Pessoa p WHERE p.idade >= 18";

        for (String jpql : aJpql) {
            JPQL analyzer = new JPQL(select + " " + jpql);
            List<String> expected = Arrays.asList("nome", "idade", "endereco.cidade.nome", "contatoEmail", "contato.fone");
            List<String> result = analyzer.extrairPropriedadesEAliasDoSelect();
            assertEquals(expected, result, "divergencia na jpql : " + jpql);
        }
    }
//    @Test
//    public void testExtrairEntidadesEAliasDoFrom() {
//        Map<String, String> result = analyzer.extrairEntidadesEAliasDoFrom();
//        assertEquals(1, result.size());
//        assertEquals("b", result.get("Bolsa"));
//    }
//    @Test
//    public void testExtrairNomesParametros() {
//        java.util.List<String> params = analyzer.extrairNomesParametros();
//        assertEquals(1, params.size());
//        assertEquals("id", params.get(0));
//    }
//    @Test
//    public void testModificarConsultaJPQL() {
//        String modification = "AND b.status = 'active'";
//        String expected = "SELECT b.id AS bid, b.name FROM Bolsa b WHERE b.id = :id AND b.status = 'active'";
//        assertEquals(expected, analyzer.modificarConsultaJPQL(modification));
//    }
//
//    @Test
//    public void testAdicionarCamposOrderBy() {
//        String orderedJPQL = analyzer.adicionarCamposOrderBy("b.date DESC");
//        String expected = "SELECT b.id AS bid, b.name FROM Bolsa b WHERE b.id = :id ORDER BY b.date DESC";
//        assertEquals(expected, orderedJPQL);
//    }
    //
//    @Test
//    void testExtrairEntidadesEAliasDosJoins1() {
//        String jpql = "SELECT p FROM Pessoa p LEFT JOIN p.endereco e INNER JOIN p.empresa AS emp FETCH JOIN p.hobbies WHERE p.idade >= 18";
//        Map<String, String> expected = new LinkedHashMap<>();
//        expected.put("p.endereco", "e");
//        expected.put("p.empresa", "emp");
//        expected.put("p.hobbies", "hobbies");
//
//        Map<String, String> result = extrairEntidadesEAliasDosJoins(jpql);
//
//        assertEquals(expected, result, "Os alias e entidades dos JOINs devem ser extraídos corretamente.");
//    }
//
//    @Test
//    void testExtrairEntidadesEAliasDosJoins2() {
//        String jpql1 = "SELECT b FROM Bolsa b INNER JOIN b.programa prg";
//        String jpql2 = "SELECT b FROM Bolsa b INNER JOIN FETCH b.programa prg";
//
//        Map<String, String> expected = new LinkedHashMap<>();
//        expected.put("b.programa", "prg");
//
//        Map<String, String> result1 = extrairEntidadesEAliasDosJoins(jpql1);
//        Map<String, String> result2 = extrairEntidadesEAliasDosJoins(jpql2);
//
//        assertEquals(expected, result1, "Os alias e entidades dos JOINs devem ser extraídos corretamente para INNER JOIN.");
//        assertEquals(expected, result2, "Os alias e entidades dos JOINs devem ser extraídos corretamente para INNER JOIN FETCH.");
//    }
//
//    @Test
//    void testExtrairEntidadesEAliasDosJoins3() {
//        String jpql = "Select b from Bolsa b inner join fetch b.programa prg inner join fetch prg.proReitoria pro left join fetch pro.responsavel1 left join fetch prg.responsavel2 where b.id = :id";
//        Map<String, String> expected = new LinkedHashMap<>();
//        expected.put("b.programa", "prg");
//        expected.put("prg.proReitoria", "pro");
//        expected.put("pro.responsavel1", "responsavel1");
//        expected.put("prg.responsavel2", "responsavel2");
//
//        Map<String, String> result = extrairEntidadesEAliasDosJoins(jpql);
//
//        assertEquals(expected, result, "Os alias e entidades dos JOINs devem ser extraídos corretamente.");
//    }
//    
//    @Test
//    void testExtrairEntidadesEAliasDosJoins() {
//        String jpql = "Select b"
//                + "  from Bolsa b"
//                + " inner join fetch b.programa prg"
//                + " inner join fetch prg.proReitoria pro"
//                + "  left join fetch pro.responsavel1"
//                + "  left join fetch prg.responsavel2"
//                //            + "  left join fetch prg.fontePagadora1"
//                //            + "  left join fetch prg.fontePagadora2"
//                + " where b.id = :id";
//        Map<String, String> expected = new LinkedHashMap<>();
//        expected.put("b.programa", "prg");
//        expected.put("prg.proReitoria", "pro");
//        expected.put("pro.responsavel1", "responsavel1");
//        expected.put("prg.responsavel2", "responsavel2");
////        expected.put("prg.fontePagadora1", "fontePagadora1");
////        expected.put("prg.fontePagadora2", "fontePagadora2");
//
//        Map<String, String> result = extrairEntidadesEAliasDosJoins(jpql);
//
//        assertEquals(expected, result, "Os alias e entidades dos JOINs devem ser extraídos corretamente.");
//    }


}
