package javax.util.spring.noap.restfull.repository.rql;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import java.util.List;

import static javax.util.spring.noap.restfull.repository.rql.SearchOperation.*;

public class ParseRestQueryLanguageTest {
    @Test
    public void testValidSimpleEqExpression() {
        ParseRestQueryLanguage parse = new ParseRestQueryLanguage("user ne active");
        List<SearchCriteria> criterias = parse.getCriterias();

        Assertions.assertEquals(1, criterias.size());
        Assertions.assertEquals("user", criterias.get(0).getKey());
        Assertions.assertEquals(NEGATION, criterias.get(0).getOperation());
        Assertions.assertEquals("active", criterias.get(0).getValue());
        Assertions.assertFalse(criterias.get(0).isOrPredicate());
    }
    
     @Test
    public void testValidSimpleEqExpressionWithFieldNested() {
        ParseRestQueryLanguage parse = new ParseRestQueryLanguage("user.name eq 'John Doe'");
        List<SearchCriteria> criterias = parse.getCriterias();

        Assertions.assertEquals(1, criterias.size());
        Assertions.assertEquals("user.name", criterias.get(0).getKey());
        Assertions.assertEquals(EQUALITY, criterias.get(0).getOperation());
        Assertions.assertEquals("John Doe", criterias.get(0).getValue());
        Assertions.assertFalse(criterias.get(0).isOrPredicate());
    }

    @Test
    public void testRegexWithGtOperator() {
        ParseRestQueryLanguage parse = new ParseRestQueryLanguage("account.balance gt 100.00");
        List<SearchCriteria> criterias = parse.getCriterias();

        Assertions.assertEquals(1, criterias.size());
        Assertions.assertEquals("account.balance", criterias.get(0).getKey());
        Assertions.assertEquals(GREATER_THAN, criterias.get(0).getOperation());
        Assertions.assertEquals("100.00", criterias.get(0).getValue());
        Assertions.assertFalse(criterias.get(0).isOrPredicate());
    }
    
    @Test
    public void testRegexWithGeOperator() {
        ParseRestQueryLanguage parse = new ParseRestQueryLanguage("account.balance ge 100.00");
        List<SearchCriteria> criterias = parse.getCriterias();

        Assertions.assertEquals(1, criterias.size());
        Assertions.assertEquals("account.balance", criterias.get(0).getKey());
        Assertions.assertEquals(GREATER_THAN_OR_EQUAL, criterias.get(0).getOperation());
        Assertions.assertEquals("100.00", criterias.get(0).getValue());
        Assertions.assertFalse(criterias.get(0).isOrPredicate());
    }
    
    @Test
    public void testRegexWithLtOperator() {
        ParseRestQueryLanguage parse = new ParseRestQueryLanguage("account.balance lt 100.00");
        List<SearchCriteria> criterias = parse.getCriterias();

        Assertions.assertEquals(1, criterias.size());
        Assertions.assertEquals("account.balance", criterias.get(0).getKey());
        Assertions.assertEquals(LESS_THAN, criterias.get(0).getOperation());
        Assertions.assertEquals("100.00", criterias.get(0).getValue());
        Assertions.assertFalse(criterias.get(0).isOrPredicate());
    }
    
     @Test
    public void testRegexWithLeOperator() {
        ParseRestQueryLanguage parse = new ParseRestQueryLanguage("account.balance le 100.00");
        List<SearchCriteria> criterias = parse.getCriterias();

        Assertions.assertEquals(1, criterias.size());
        Assertions.assertEquals("account.balance", criterias.get(0).getKey());
        Assertions.assertEquals(LESS_THAN_OR_EQUAL, criterias.get(0).getOperation());
        Assertions.assertEquals("100.00", criterias.get(0).getValue());
        Assertions.assertFalse(criterias.get(0).isOrPredicate());
    }
    
    @Test
    public void testRegexWithLikeOperator() {
        ParseRestQueryLanguage parse = new ParseRestQueryLanguage("address.city like 'New York'");
        List<SearchCriteria> criterias = parse.getCriterias();

        Assertions.assertEquals(1, criterias.size());
        Assertions.assertEquals("address.city", criterias.get(0).getKey());
        Assertions.assertEquals(LIKE, criterias.get(0).getOperation());
        Assertions.assertEquals("New York", criterias.get(0).getValue());
        Assertions.assertFalse(criterias.get(0).isOrPredicate());
    }
    
     @Test
    public void testRegexWithAsteriskValue() {
        ParseRestQueryLanguage parse = new ParseRestQueryLanguage("file.name like *.txt");
        List<SearchCriteria> criterias = parse.getCriterias();

        Assertions.assertEquals(1, criterias.size());
        Assertions.assertEquals("file.name", criterias.get(0).getKey());
        Assertions.assertEquals(LIKE, criterias.get(0).getOperation());
        Assertions.assertEquals("*.txt", criterias.get(0).getValue());
        Assertions.assertFalse(criterias.get(0).isOrPredicate());
    }
    
    @Test
    public void testRegexWithEqOperatorAndPredicateOR() {
        ParseRestQueryLanguage parse = new ParseRestQueryLanguage("field1 eq value1 and field2 gt value2 or field3 lt value3");
        List<SearchCriteria> criterias = parse.getCriterias();

        Assertions.assertEquals(3, criterias.size());

        Assertions.assertEquals("field1", criterias.get(0).getKey());
        Assertions.assertEquals(EQUALITY, criterias.get(0).getOperation());
        Assertions.assertEquals("value1", criterias.get(0).getValue());
        Assertions.assertFalse(criterias.get(0).isOrPredicate());

        Assertions.assertEquals("field2", criterias.get(1).getKey());
        Assertions.assertEquals(GREATER_THAN, criterias.get(1).getOperation());
        Assertions.assertEquals("value2", criterias.get(1).getValue());
        Assertions.assertFalse(criterias.get(1).isOrPredicate());

        Assertions.assertEquals("field3", criterias.get(2).getKey());
        Assertions.assertEquals(LESS_THAN, criterias.get(2).getOperation());
        Assertions.assertEquals("value3", criterias.get(2).getValue());
        Assertions.assertTrue(criterias.get(2).isOrPredicate());
    }

    @Test
    public void testInvalidQuery() {
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new ParseRestQueryLanguage("field1 value1[and]field2 gt value2[or]field3 lt"),
                "Expected to throw, but it didn't"
        );

        Assertions.assertTrue(thrown.getMessage().contains(ParseRestQueryLanguage.SINTAX_ERRO));
    }

    @Test
    public void testDifferentOperations() {
        ParseRestQueryLanguage parse = new ParseRestQueryLanguage("field1 ne value1 and field2 ge value2 or field3 le value3");
        List<SearchCriteria> criterias = parse.getCriterias();

        Assertions.assertEquals(3, criterias.size());

        Assertions.assertEquals("field1", criterias.get(0).getKey());
        Assertions.assertEquals(NEGATION, criterias.get(0).getOperation());
        Assertions.assertEquals("value1", criterias.get(0).getValue());
        Assertions.assertFalse(criterias.get(0).isOrPredicate());

        Assertions.assertEquals("field2", criterias.get(1).getKey());
        Assertions.assertEquals(GREATER_THAN_OR_EQUAL, criterias.get(1).getOperation());
        Assertions.assertEquals("value2", criterias.get(1).getValue());
        Assertions.assertFalse(criterias.get(1).isOrPredicate());

        Assertions.assertEquals("field3", criterias.get(2).getKey());
        Assertions.assertEquals(LESS_THAN_OR_EQUAL, criterias.get(2).getOperation());
        Assertions.assertEquals("value3", criterias.get(2).getValue());
        Assertions.assertTrue(criterias.get(2).isOrPredicate());
    }

    @Test
    public void testInvalidValueEmpty() {
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new ParseRestQueryLanguage("field1 like"),
                "Expected to throw, but it didn't"
        );

        Assertions.assertTrue(thrown.getMessage().contains(ParseRestQueryLanguage.SINTAX_ERRO));
    }

    @Test
    public void testInvalidValueEmptyWithSpace() {
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new ParseRestQueryLanguage("field1 like "),
                "Expected to throw, but it didn't"
        );

        Assertions.assertTrue(thrown.getMessage().contains(ParseRestQueryLanguage.SINTAX_ERRO));
    }

    @Test
    public void testEmptyValue() {
        ParseRestQueryLanguage parse = new ParseRestQueryLanguage("field1 like ''");
        List<SearchCriteria> criterias = parse.getCriterias();

        Assertions.assertEquals(1, criterias.size());

        Assertions.assertEquals("field1", criterias.get(0).getKey());
        Assertions.assertEquals(LIKE, criterias.get(0).getOperation());
        Assertions.assertEquals("", criterias.get(0).getValue());
        Assertions.assertFalse(criterias.get(0).isOrPredicate());
    }

    @Test
    public void testInvalidValueEmptyWithSpace2() {
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new ParseRestQueryLanguage("field1 eq  and field2 gt value2 or field3 lt "),
                "Expected to throw, but it didn't"
        );

        Assertions.assertTrue(thrown.getMessage().contains(ParseRestQueryLanguage.SINTAX_ERRO + "Erro próximo a: field1 eq  and"));
    }

    @Test
    public void testInvalidValueEmptyWithSpace3() {
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new ParseRestQueryLanguage("field1 eq '' and field2 gt value2 or field3 lt "),
                "Expected to throw, but it didn't"
        );

        Assertions.assertTrue(thrown.getMessage().contains(ParseRestQueryLanguage.SINTAX_ERRO + "Erro próximo a: field3 lt "));
    }
    
     @Test
    public void testFieldNestedInvalidValueEmpty() {
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new ParseRestQueryLanguage("address.city like"),
                "Expected to throw, but it didn't"
        );

        Assertions.assertTrue(thrown.getMessage().contains(ParseRestQueryLanguage.SINTAX_ERRO + "Erro próximo a: address.city"));
    }

    @Test
    public void testEmptyValue3() {
        ParseRestQueryLanguage parse = new ParseRestQueryLanguage("field1 eq '' and field2 gt value2 or field3 lt 100");
        List<SearchCriteria> criterias = parse.getCriterias();

        Assertions.assertEquals(3, criterias.size());

        Assertions.assertEquals("field1", criterias.get(0).getKey());
        Assertions.assertEquals(EQUALITY, criterias.get(0).getOperation());
        Assertions.assertEquals("", criterias.get(0).getValue());
        Assertions.assertFalse(criterias.get(0).isOrPredicate());

        Assertions.assertEquals("field2", criterias.get(1).getKey());
        Assertions.assertEquals(GREATER_THAN, criterias.get(1).getOperation());
        Assertions.assertEquals("value2", criterias.get(1).getValue());
        Assertions.assertFalse(criterias.get(1).isOrPredicate());

        Assertions.assertEquals("field3", criterias.get(2).getKey());
        Assertions.assertEquals(LESS_THAN, criterias.get(2).getOperation());
        Assertions.assertEquals("100", criterias.get(2).getValue());
        Assertions.assertTrue(criterias.get(2).isOrPredicate());
    }

    @Test
    public void testNestedFields() {
        ParseRestQueryLanguage parse = new ParseRestQueryLanguage("field1.subfield eq value1 and field2.subfield gt value2");
        List<SearchCriteria> criterias = parse.getCriterias();

        Assertions.assertEquals(2, criterias.size());

        Assertions.assertEquals("field1.subfield", criterias.get(0).getKey());
        Assertions.assertEquals(EQUALITY, criterias.get(0).getOperation());
        Assertions.assertEquals("value1", criterias.get(0).getValue());
        Assertions.assertFalse(criterias.get(0).isOrPredicate());

        Assertions.assertEquals("field2.subfield", criterias.get(1).getKey());
        Assertions.assertEquals(GREATER_THAN, criterias.get(1).getOperation());
        Assertions.assertEquals("value2", criterias.get(1).getValue());
        Assertions.assertFalse(criterias.get(1).isOrPredicate());
    }

    @Test
    public void testMultipleSpacesInValue() {
        ParseRestQueryLanguage parse = new ParseRestQueryLanguage("field1 eq 'value 1 with spaces' and field2 gt value2");
        List<SearchCriteria> criterias = parse.getCriterias();

        Assertions.assertEquals(2, criterias.size());

        Assertions.assertEquals("field1", criterias.get(0).getKey());
        Assertions.assertEquals(EQUALITY, criterias.get(0).getOperation());
        Assertions.assertEquals("value 1 with spaces", criterias.get(0).getValue());
        Assertions.assertFalse(criterias.get(0).isOrPredicate());

        Assertions.assertEquals("field2", criterias.get(1).getKey());
        Assertions.assertEquals(GREATER_THAN, criterias.get(1).getOperation());
        Assertions.assertEquals("value2", criterias.get(1).getValue());
        Assertions.assertFalse(criterias.get(1).isOrPredicate());
    }

    @Test
    public void testAsterisksInValue() {
        ParseRestQueryLanguage parse = new ParseRestQueryLanguage("field1 eq *value* and field2 gt *value2*");
        List<SearchCriteria> criterias = parse.getCriterias();

        Assertions.assertEquals(2, criterias.size());

        Assertions.assertEquals("field1", criterias.get(0).getKey());
        Assertions.assertEquals(EQUALITY, criterias.get(0).getOperation());
        Assertions.assertEquals("*value*", criterias.get(0).getValue());
        Assertions.assertFalse(criterias.get(0).isOrPredicate());

        Assertions.assertEquals("field2", criterias.get(1).getKey());
        Assertions.assertEquals(GREATER_THAN, criterias.get(1).getOperation());
        Assertions.assertEquals("*value2*", criterias.get(1).getValue());
        Assertions.assertFalse(criterias.get(1).isOrPredicate());
    }
}
