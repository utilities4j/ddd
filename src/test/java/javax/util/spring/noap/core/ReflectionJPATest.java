package javax.util.spring.noap.core;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Marcius
 */
public class ReflectionJPATest {

    public ReflectionJPATest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getIdProperty method, of class ReflectionJPA.
     */
    @Test
    public void testGetIdProperty() {
        System.out.println("getIdProperty");
        Object object = new MyClass(1);
        PropertyId.IdProperty expResult = new PropertyId.IdProperty("idMuClass", 1, false);
        PropertyId.IdProperty result = PropertyId.getIdProperty(object);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetIdPropertyWithGeneratedValue() {
        System.out.println("getIdProperty");
        Object object = new MyClassWithGeneratedValue();
        PropertyId.IdProperty expResult = new PropertyId.IdProperty("id", null, true);
        PropertyId.IdProperty result = PropertyId.getIdProperty(object);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetIdPropertyOfSubEntity() {
        System.out.println("getIdPropertyOfSubEntity");
        Object object = new MySubClass(1);
        PropertyId.IdProperty expResult = new PropertyId.IdProperty("idMyBaseClass", 1, false);
        PropertyId.IdProperty result = PropertyId.getIdProperty(object);
        assertEquals(expResult, result);
    }

    /**
     * Test of getIdFieldName method, of class ReflectionJPA.
     */
    @Test
    public void testGetIdFieldName() {
        System.out.println("getIdFieldName");
        Class clazz = MySubClass.class;
        String expResult = "idMyBaseClass";
        String result = PropertyId.getIdFieldName(clazz);
        assertEquals(expResult, result);
    }

}

class MyClass {

    @Id
    private int idMuClass;

    public int getIdMuClass() {
        return idMuClass;
    }

    public void setIdMuClass(int idMuClass) {
        this.idMuClass = idMuClass;
    }

    public MyClass(int idMuClass) {
        this.idMuClass = idMuClass;
    }

    public MyClass() {
    }

}

class MyClassWithGeneratedValue {

    @Id
    @GeneratedValue
    private Integer id;

}

class MyBaseClass {

    @Id
    protected int idMyBaseClass;

    public int getIdMyBaseClass() {
        return idMyBaseClass;
    }

    public void setIdMyBaseClass(int idMyBaseClass) {
        this.idMyBaseClass = idMyBaseClass;
    }

    public MyBaseClass(int idMyBaseClass) {
        this.idMyBaseClass = idMyBaseClass;
    }

    public MyBaseClass() {
    }

}

class MySubClass extends MyBaseClass {

    private String detail;

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public MySubClass(int idMyBaseClass) {
        super(idMyBaseClass);
    }

    public MySubClass() {
    }

}
