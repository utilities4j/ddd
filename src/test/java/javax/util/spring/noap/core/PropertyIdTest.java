package  javax.util.spring.noap.core;

import jakarta.persistence.Id;
import static org.junit.Assert.*;

import org.junit.Test;

public class PropertyIdTest {

    // Teste para verificar se a propriedade identificadora é corretamente obtida para uma entidade.
    @Test
    public void testGetIdProperty() {
        // Cria uma entidade de exemplo com um ID definido.
        ExampleEntity entity = new ExampleEntity(1L, "example");

        // Obtém a propriedade identificadora da entidade.
        PropertyId.IdProperty idProperty = PropertyId.getIdProperty(entity);

        // Verifica se a propriedade identificadora foi obtida corretamente.
        assertNotNull(idProperty);
        assertEquals("idEntity", idProperty.name());
        assertEquals(1L, idProperty.value());
        assertFalse(idProperty.generatedValue());
    }

    // Teste para verificar se o nome do campo identificador é corretamente obtido para uma classe.
    @Test
    public void testGetIdFieldName() {
        // Obtém o nome do campo identificador para a classe de exemplo.
        String idFieldName = PropertyId.getIdFieldName(ExampleEntity.class);

        // Verifica se o nome do campo identificador foi obtido corretamente.
        assertNotNull(idFieldName);
        assertEquals("idEntity", idFieldName);
    }

    // Teste para verificar se o valor do campo identificador é corretamente obtido para uma entidade.
    @Test
    public void testGetIdFieldValue() throws IllegalAccessException {
        // Cria uma entidade de exemplo com um ID definido.
        ExampleEntity entity = new ExampleEntity(1L, "example");

        // Obtém o valor do campo identificador da entidade.
        Object idFieldValue = PropertyId.getIdFieldValue(entity);

        // Verifica se o valor do campo identificador foi obtido corretamente.
        assertNotNull(idFieldValue);
        assertEquals(1L, idFieldValue);
    }

    // Entidade de exemplo para testes.
    private static class ExampleEntity {

        @Id
        private Long idEntity;
        private String name;

        public ExampleEntity(Long id, String name) {
            this.idEntity = id;
            this.name = name;
        }
    }

    // Teste para verificar o conteúdo da mensagem da exceção ao tentar obter a propriedade identificadora de uma entidade sem ID.
    @Test
    public void testGetIdPropertyExceptionMessage() {
        // Cria uma entidade de exemplo sem um campo identificador anotado com @Id.
        ExampleEntityWithoutId entity = new ExampleEntityWithoutId();

        try {
            PropertyId.getIdProperty(entity);
            fail("Expected RuntimeException to be thrown");
        } catch (RuntimeException e) {
            // Verifica se a mensagem da exceção contém o texto esperado.
            assertTrue(e.getMessage().contains("has no property annotated with @Id"));
        }
    }

    // Teste para verificar o conteúdo da mensagem da exceção ao tentar obter o nome do campo identificador de uma classe sem ID.
    @Test
    public void testGetIdFieldNameExceptionMessage() {
        try {
            // Tenta obter o nome do campo identificador de uma classe sem ID.
            PropertyId.getIdFieldName(ExampleEntityWithoutId.class);
            fail("Expected RuntimeException to be thrown");
        } catch (RuntimeException e) {
            // Verifica se a mensagem da exceção contém o texto esperado.
            assertTrue(e.getMessage().contains("No field annotated with @Id found in class hierarchy"));
        }
    }

    // Teste para verificar o conteúdo da mensagem da exceção ao tentar obter o valor do campo identificador de uma entidade sem ID.
    @Test
    public void testGetIdFieldValueExceptionMessage() throws IllegalAccessException {
        // Cria uma entidade de exemplo sem um campo identificador anotado com @Id.
        ExampleEntityWithoutId entity = new ExampleEntityWithoutId();

        try {
            PropertyId.getIdFieldValue(entity);
            fail("Expected RuntimeException to be thrown");
        } catch (RuntimeException e) {
            // Verifica se a mensagem da exceção contém o texto esperado.
            assertTrue(e.getMessage().contains("No field annotated with @Id found in class hierarchy of"));
        } 
    }

    // Entidade de exemplo sem campo identificador para testes.
    private static class ExampleEntityWithoutId {

        private String name;
    }
}
