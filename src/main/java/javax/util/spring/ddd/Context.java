package javax.util.spring.ddd;

import javax.util.ddd.domain.IRepository;

public class Context {

    public static IRepository getRepository() {
        return ApplicationContextProvider.getApplicationContext().getBean(IRepository.class);
    }

    public static <T> T getBean(String name, Class<T> requiredType) {
        return ApplicationContextProvider.getApplicationContext().getBean(name, requiredType);
    }
}
