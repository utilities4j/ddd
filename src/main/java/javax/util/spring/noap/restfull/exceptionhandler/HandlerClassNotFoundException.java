package javax.util.spring.noap.restfull.exceptionhandler;

import org.springframework.http.HttpStatus;

public class HandlerClassNotFoundException implements HandlerException {

    @Override
    public boolean canHandle(Throwable exception) {
        return exception instanceof ClassNotFoundException;
    }

    @Override
    public ProblemDetail handle(Throwable exception) {
        return ProblemDetail
                .builder()
                .status(HttpStatus.NOT_FOUND.value())
                .title("Recurso não encontrado")
                .detail(exception.getMessage())
                .build();
    }
}
