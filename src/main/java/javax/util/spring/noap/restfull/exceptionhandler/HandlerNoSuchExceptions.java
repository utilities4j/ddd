package javax.util.spring.noap.restfull.exceptionhandler;

import java.util.NoSuchElementException;
import org.springframework.http.HttpStatus;

public class HandlerNoSuchExceptions implements HandlerException {

    @Override
    public boolean canHandle(Throwable exception) {
        return exception instanceof NoSuchElementException
                || exception instanceof NoSuchMethodException
                || exception instanceof NoSuchFieldException
                || exception instanceof NoSuchFieldError;
    }

    @Override
    public ProblemDetail handle(Throwable exception) {
        return ProblemDetail
                .builder()
                .status(HttpStatus.NOT_FOUND.value())
                //.title("Recurso não encontrado")
                .detail(exception.getMessage())
                .build();
    }
}
