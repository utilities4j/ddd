package javax.util.spring.noap.restfull.exceptionhandler;

import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static org.springframework.http.HttpStatus.*;

public class HandlerSQLException implements HandlerException {

    @Override
    public boolean canHandle(Throwable exception) {
        return exception instanceof SQLException;
    }

    @Override
    public ProblemDetail handle(Throwable exception) {

        ProblemDetail problemDetail = new ProblemDetail();
        problemDetail.setStatus(INTERNAL_SERVER_ERROR);        
        problemDetail.setTitle("Erro Interno no Banco de Dados");
        problemDetail.setDetail(exception.getMessage());

        SQLException sqlException = (SQLException) exception;
        String sqlState = String.valueOf(sqlException.getSQLState());

        //https://www.ibm.com/docs/en/db2-for-zos/12?topic=codes-sqlstate-values-common-error
        if (sqlState.startsWith("23")) {
            //Constraint Violation                
            problemDetail.setStatus(CONFLICT);
            String messagePart = extractTextBetweenParentheses(sqlException.getMessage());
            problemDetail.setDetail(messagePart.isEmpty() ? sqlException.getMessage() : messagePart);
            problemDetail.getDetails().add(sqlException.getMessage());

            switch (sqlState) {
                case "23505" -> {
                    //Violação de restrição de chave única ou de índice exclusivo.                    
                    problemDetail.setTitle("Informação já cadastrada");
                }
                case "23503" -> {
                    // Violação de restrição de chave estrangeira.                                        
                    problemDetail.setTitle("Operação inválida");
                }
            }
        }

        return problemDetail;
    }

    private String extractTextBetweenParentheses(String input) {
        String regex = "\\(([^)]+)\\)=\\(([^)]+)\\)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        if (matcher.find()) {
            String firstPart = matcher.group(1);
            String secondPart = matcher.group(2);
            return firstPart + " = " + secondPart;
        } else {
            return "";
        }
    }
}
