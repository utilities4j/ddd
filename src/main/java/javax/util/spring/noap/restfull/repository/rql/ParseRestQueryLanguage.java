package javax.util.spring.noap.restfull.repository.rql;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Rest Query language
 *
 * http://localhost:8080/users?filter=lastName eq doe or age gt 25
 *
 *
 * @author Marcius Brandão
 */
public class ParseRestQueryLanguage {

    /**
     * <pre>
     *
     * ([\\w.]+): Captura o campo, composto por letras, números, sublinhados (_) e pontos (.).
     * \\[(:|eq|ne|>|gt|>:|ge|<|lt|<:|le|~|like)\\]: Captura o operador dentro dos colchetes ([]).
     *                                               Os operadores permitidos são :, eq, ne, >, gt, >:, ge, <, lt, <:, le, ~, like.
     * ([\\w.\\s\\*]*?): Captura o valor, que pode ser composto por letras, números, sublinhados, pontos, espaços e *.
     *                   O *? permite que essa parte seja opcional e o ? torna a captura não-gulosa,
     *                   ou seja, capturará o menor número possível de caracteres.
     * (\\[and\\]|\\[or\\]): Captura o operador lógico and ou or dentro dos colchetes ([]).
     * </pre>
     *
     */
    static final String REGEX = "([\\w.]+)\\s+(:|eq|ne|>|gt|>:|ge|<|lt|<:|le|~|like)\\s*(?:'([^']*|\\s*)'|([^\\s]+))\\s*(\\sand\\s+|\\s+or\\s+|$)";

    static final Pattern PATTERN = Pattern.compile(REGEX);

    private final List<SearchCriteria> criterias = new ArrayList<>();

    public ParseRestQueryLanguage(String search) {
        Matcher matcher = PATTERN.matcher(search);
        throwIfInvalidSintax(search);

        if (!matcher.find()) {
            throw new IllegalArgumentException(SINTAX_ERRO);
        }

        String orPredicate = "";
        do {
            String value;
            if (matcher.group(3) != null) {
                value = matcher.group(3);
                // Remover aspas simples do início e do fim, se presentes
                if (value.startsWith("'") && value.endsWith("'")) {
                    value = value.substring(1, value.length() - 1);
                }
            } else {
                value = matcher.group(4);
            }

            criterias.add(new SearchCriteria.Builder()
                    .orPredicate(orPredicate)
                    .key(matcher.group(1))
                    .operation(matcher.group(2))
                    .value(value)
                    .build());
            orPredicate = matcher.group(5);
        } while (matcher.find());
    }

    static final String SINTAX_ERRO = "Sintaxe incorreta do parametro filter:";

    public List<SearchCriteria> getCriterias() {
        return criterias;
    }

    private void throwIfInvalidSintax(String search) {
        Matcher matcher = PATTERN.matcher(search);
        int position = 0;

        while (position < search.length()) {
            if (matcher.find(position)) {
                if (matcher.start() != position) {
                    throw new IllegalArgumentException(SINTAX_ERRO + "Erro próximo a: " + search.substring(position, Math.min(position + 15, search.length())));
                }
                position = matcher.end();
            } else {
                throw new IllegalArgumentException(SINTAX_ERRO + "Erro próximo a: " + search.substring(position, Math.min(position + 15, search.length())));
            }
        }
    }

}
