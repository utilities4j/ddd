package javax.util.spring.noap.restfull.repository.rql;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import java.util.List;
import java.util.function.Consumer;

public class SearchQueryCriteriaConsumer implements Consumer<SearchCriteria> {

    private final Root<?> root;
    private Predicate predicate;
    private final CriteriaBuilder criteriaBuilder;
    private final List<SearchCriteria> searchCriterias;

    public SearchQueryCriteriaConsumer(CriteriaBuilder criteriaBuilder, Root<?> root, String filter) {
        this.root = root;
        this.criteriaBuilder = criteriaBuilder;
        this.predicate = criteriaBuilder.conjunction();

        this.searchCriterias = new ParseRestQueryLanguage(filter).getCriterias();
    }

    public Predicate getPredicate() {
        searchCriterias.forEach(this);
        return predicate;
    }

    @Override
    public void accept(SearchCriteria criteria) {
        String fieldName = criteria.getKey();
        Object value = criteria.getValue();

        Path<?> path = getPath(fieldName);
        if (criteria.isOrPredicate()) {
            switch (criteria.getOperation()) {
                case EQUALITY ->
                    predicate = criteriaBuilder.or(predicate, criteriaBuilder.equal(path, value));
                case NEGATION ->
                    predicate = criteriaBuilder.or(predicate, criteriaBuilder.notEqual(path, value));
                case GREATER_THAN ->
                    predicate = criteriaBuilder.or(predicate, criteriaBuilder.greaterThan(path.as(String.class), value.toString()));
                case LESS_THAN ->
                    predicate = criteriaBuilder.or(predicate, criteriaBuilder.lessThan(path.as(String.class), value.toString()));
                case LIKE -> {
                    String newValue = value.toString().replaceAll("\\*", "%");
                    if (newValue.equals(value)) {
                        predicate = criteriaBuilder.or(predicate, criteriaBuilder.like(path.as(String.class), "%" + newValue + "%"));
                    } else {
                        predicate = criteriaBuilder.or(predicate, criteriaBuilder.like(path.as(String.class), newValue));
                    }
                }
                case GREATER_THAN_OR_EQUAL ->
                    predicate = criteriaBuilder.or(predicate, criteriaBuilder.greaterThanOrEqualTo(path.as(String.class), value.toString()));
                case LESS_THAN_OR_EQUAL ->
                    predicate = criteriaBuilder.or(predicate, criteriaBuilder.lessThanOrEqualTo(path.as(String.class), value.toString()));
                default -> {
                }
            }
        } else {
            switch (criteria.getOperation()) {
                case EQUALITY ->
                    predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(path, value));
                case NEGATION ->
                    predicate = criteriaBuilder.and(predicate, criteriaBuilder.notEqual(path, value));
                case GREATER_THAN ->
                    predicate = criteriaBuilder.and(predicate, criteriaBuilder.greaterThan(path.as(String.class), value.toString()));
                case LESS_THAN ->
                    predicate = criteriaBuilder.and(predicate, criteriaBuilder.lessThan(path.as(String.class), value.toString()));
                case LIKE -> {
                    String newValue = value.toString().replaceAll("\\*", "%");
                    if (newValue.equals(value)) {
                        predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(path.as(String.class), "%" + newValue + "%"));
                    } else {
                        predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(path.as(String.class), newValue));
                    }
                }
                case GREATER_THAN_OR_EQUAL ->
                    predicate = criteriaBuilder.and(predicate, criteriaBuilder.greaterThanOrEqualTo(path.as(String.class), value.toString()));
                case LESS_THAN_OR_EQUAL ->
                    predicate = criteriaBuilder.and(predicate, criteriaBuilder.lessThanOrEqualTo(path.as(String.class), value.toString()));
                default -> {
                }
            }
        }
    }

    /**
     * percorrer os campos aninhados. Ele divide o campo pelo ponto (.) e obtém cada parte do Root
     * ou do Path correspondente. permitindo que a lógica continue a funcionar para campos simples e
     * aninhados.
     *
     * @param fieldName
     * @return
     */
    private Path<?> getPath(String fieldName) {
        String[] fields = fieldName.split("\\.");
        Path<?> path = root;
        for (String field : fields) {
            path = path.get(field);
        }
        return path;
    }
}
