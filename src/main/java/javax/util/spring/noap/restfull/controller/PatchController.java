package javax.util.spring.noap.restfull.controller;

import jakarta.persistence.EntityExistsException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import javax.util.spring.noap.core.PropertyId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.support.TransactionTemplate;

import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller ainda em desenvolvimento
 *
 * @author Marcius
 * @version 1.0
 */
@RestController
@RequestMapping(value = "/domain")
public class PatchController extends BaseController {

    @Autowired
    private TransactionTemplate transactionTemplate;

    @PatchMapping("/{entityName}/{id}")
    public ResponseEntity<?> update(
            @PathVariable(name = "entityName", required = true) String entityName,
            @PathVariable(name = "id", required = true) Object id,
            @RequestParam(name = "fields", required = false) List<String> fields,
            @RequestBody(required = true) String jsonString) throws Exception {

        Class<?> entityClass = findEntityClassOrThrow(entityName);

        return transactionTemplate.execute(status -> {
            try {
                Object entity = findEntityInstanceOrThrow(entityClass, id);
                jsonToObject(jsonString, entity);
                getEntityManager().merge(entity);

                if (fields != null && !fields.isEmpty()) {
                    Map<String, Object> responseMap = new LinkedHashMap<>();
                    PropertyId.IdProperty idField = PropertyId.getIdProperty(entity);
                    Object updatedEntity = getEntityManager().find(entityClass, idField.value()); // @TODO otimizar trazendo apenas os campos solicitados
                    responseMap.putAll(getSelectedFields(updatedEntity, fields));
                    return ResponseEntity.ok().body(responseMap);
                } else {
                    return ResponseEntity.noContent().build();
                }
            } catch (Exception e) {
                status.setRollbackOnly();
                throw new RuntimeException(e);
            }
        });
    }

    @PatchMapping("/{entityName}/{entityId}/{collectionFieldName}/add")
    public ResponseEntity<Object> addToList(
            @PathVariable(name = "entityName", required = true) String entityName,
            @PathVariable(name = "entityId", required = true) Long entityId,
            @PathVariable(name = "collectionFieldName", required = true) String collectionFieldName,
            @RequestBody(required = true) String itemJsonString) throws Exception {

        Class<?> entityClass = findEntityClassOrThrow(entityName);

        return transactionTemplate.execute(status -> {
            try {
                Object entity = findEntityInstanceOrThrow(entityClass, entityId);
                addToCollection(entity, collectionFieldName, itemJsonString);
                return ResponseEntity.noContent().build();
            } catch (Exception e) {
                status.setRollbackOnly();
                throw new RuntimeException(e);
            }
        });
    }

    public void addToCollection(Object entity, String collectionFieldName, String properties) throws Exception {
        // Verifica se os parâmetros não são nulos
        if (entity == null || collectionFieldName == null || properties == null) {
            throw new IllegalArgumentException("Os parâmetros entity, collectionFieldName e properties não podem ser nulos.");
        }

        // Obtém o campo correspondente à coleção
        Field collectionField = entity.getClass().getDeclaredField(collectionFieldName);
        collectionField.setAccessible(true);

        // Obtém o tipo dos elementos da coleção
        ParameterizedType collectionGenericType = (ParameterizedType) collectionField.getGenericType();
        Class<?> collectionType = (Class<?>) collectionGenericType.getActualTypeArguments()[0];

        // Converte o mapa no tipo necessário
        Object item = collectionType.getDeclaredConstructor().newInstance();
        jsonToObject(properties, item);

        // Inicializa a coleção se estiver nula
        Collection<Object> collection = (Collection<Object>) collectionField.get(entity);
        if (collection == null) {
            collection = new ArrayList<>();
            collectionField.set(entity, collection);
        }

        // Adiciona o item à coleção e persiste a entidade
        if (collection.add(item)) {
            getEntityManager().merge(entity);
            getEntityManager().flush(); 
        } else {
            throw new EntityExistsException(collectionType.getSimpleName() + " já contém o item: " + item.toString());
        }
    }

    @PatchMapping("/{entityName}/{entityId}/{collectionFieldName}/remove/{itemId}")
    public ResponseEntity<Object> removeFromList(
            @PathVariable(name = "entityName", required = true) String entityName,
            @PathVariable(name = "entityId", required = true) Long entityId,
            @PathVariable(name = "collectionFieldName", required = true) String collectionFieldName,
            @PathVariable(name = "itemId", required = true) Long itemId) throws Exception {

        Class<?> entityClass = findEntityClassOrThrow(entityName);

        return transactionTemplate.execute(status -> {
            try {
                Object entity = findEntityInstanceOrThrow(entityClass, entityId);
                removeFromCollection(entity, collectionFieldName, itemId);
                return ResponseEntity.noContent().build();
            } catch (Exception e) {
                status.setRollbackOnly();
                throw new RuntimeException(e);
            }
        });
    }

    public void removeFromCollection(Object entity, String collectionFieldName, Object itemId) throws Exception {
        // Obtém o campo correspondente à coleção
        Field collectionField = entity.getClass().getDeclaredField(collectionFieldName);
        collectionField.setAccessible(true);

        // Verifica se a coleção está inicializada
        Collection<Object> collection = (Collection<Object>) collectionField.get(entity);
        if (collection == null || collection.isEmpty()) {
            throw new NoSuchElementException("A coleção '" + collectionFieldName + "' está vazia ou não foi inicializada.");
        }

        Object firstItem = collection.iterator().next();
        Field idField = PropertyId.getIdField(firstItem.getClass());
        idField.setAccessible(true);

        // Encontra o item a ser removido
        Optional<Object> itemToRemove = collection.stream()
                .filter(item -> {
                    try {
                        return idField.get(item).equals(itemId);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException("Erro ao acessar o valor do campo 'id'.", e);
                    }
                }).findFirst();

        // Remove o item da coleção, se encontrado
        if (itemToRemove.isPresent() && collection.remove(itemToRemove.get())) {
            getEntityManager().merge(entity); // Persiste a entidade com a coleção atualizada
        } else {
            // Item não encontrado ou não removido
            throw new NoSuchElementException("Item com ID " + itemId + " não encontrado na coleção '" + collectionFieldName + "'.");
        }
    }

    @PatchMapping("/{entityName}/{entityId}/{collectionFieldName}/add/{itemId}")
    public ResponseEntity<Object> addToListById(
            @PathVariable(name = "entityName", required = true) String entityName,
            @PathVariable(name = "entityId", required = true) Long entityId,
            @PathVariable(name = "collectionFieldName", required = true) String collectionFieldName,
            @PathVariable(name = "itemId", required = true) Object itemId) throws Exception {

        Class<?> entityClass = findEntityClassOrThrow(entityName);

        return transactionTemplate.execute(status -> {
            try {
                Object entity = findEntityInstanceOrThrow(entityClass, entityId);

                addItemToCollectionById(entity, collectionFieldName, itemId);
                return ResponseEntity.noContent().build();
            } catch (Exception e) {
                status.setRollbackOnly();
                throw new RuntimeException(e);
            }
        });
    }

    public void addItemToCollectionById(Object entity, String collectionFieldName, Object itemId) throws Exception {
        // Verifica se os parâmetros não são nulos
        if (entity == null || collectionFieldName == null || itemId == null) {
            throw new IllegalArgumentException("Os parâmetros entity, collectionFieldName e itemId não podem ser nulos.");
        }

        // Obtém o campo correspondente à coleção
        Field collectionField = entity.getClass().getDeclaredField(collectionFieldName);
        collectionField.setAccessible(true);

        // Obtém o tipo dos elementos da coleção
        ParameterizedType collectionGenericType = (ParameterizedType) collectionField.getGenericType();
        Class<?> collectionType = (Class<?>) collectionGenericType.getActualTypeArguments()[0];

        // Encontra a instância do item a ser adicionado
        Object item = findEntityInstanceOrThrow(collectionType, itemId);

        // Obtém a coleção e adiciona o item
        Collection<Object> collection = (Collection<Object>) collectionField.get(entity);
        if (collection == null) {
            // Inicializa a coleção se estiver null
            collection = new ArrayList<>();
            collectionField.set(entity, collection); // Atualiza o campo da coleção na entidade
        }

        if (collection.add(item)) {
            getEntityManager().merge(entity); // Persiste a entidade com a coleção atualizada
        } else {
            throw new EntityExistsException(collectionFieldName + " já contém o item (" + itemId + ")");
        }
    }

}
