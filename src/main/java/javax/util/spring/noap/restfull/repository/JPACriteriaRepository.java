package javax.util.spring.noap.restfull.repository;

import jakarta.persistence.Embedded;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Query;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.From;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Order;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Root;
import jakarta.persistence.criteria.Selection;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.util.ddd.infra.PageImpl;
import javax.util.ddd.infra.util.Throw;
import javax.util.spring.noap.core.PropertyId;
import javax.util.spring.noap.core.ResultProcessor;
import javax.util.spring.noap.restfull.controller.BaseController;
import javax.util.spring.noap.restfull.repository.rql.SearchQueryCriteriaConsumer;

/**
 *
 * @author Marcius Brandão
 */
public class JPACriteriaRepository extends BaseController {

    public List<Object[]> queryEntities(Class<?> entityClass, List<String> fields, String filter) {
        return queryEntities(entityClass, fields, null, -1, -1, filter, null);
    }

//    /**
//     * Cria as seleções para os campos especificados, fazendo left join nas associações não
//     * obrigatórias.
//     *
//     * @param root
//     * @param field
//     * @param cb
//     * @param joins
//     * @return
//     */
    private List<Selection<?>> createSelections(From<?, ?> root, List<String> fields) {
        Map<String, Join<?, ?>> joins = new LinkedHashMap<>();
        List<Selection<?>> selections = new ArrayList<>();
        for (String field : fields) {
            selections.add(createSelection(root, field, joins));
        }
        return selections;
    }

    private Selection<?> createSelection(From<?, ?> root, String field, Map<String, Join<?, ?>> joins) {
        Path<?> path = root;
        Join<?, ?> join = null;
        String[] nestedFields = field.split("\\.");

        // StringBuilder para construir as chaves de join de forma incremental
        StringBuilder joinKey = new StringBuilder();

        for (int i = 0; i < nestedFields.length; i++) {
            String nestedField = nestedFields[i];
            joinKey.append(nestedField);

            boolean isAssociation = isAssociation(path, nestedField);

            if (isAssociation) {
                // Verifica se já existe um join para a chave atual                
                String key = joinKey.toString();
                if (joins.containsKey(key)) {
                    join = joins.get(key);
                } else {
                    if (join == null) {
                        join = root.join(nestedField, JoinType.LEFT);
                    } else {
                        join = join.join(nestedField, JoinType.LEFT);
                    }
                    joins.put(key, join);
                }
                path = joins.get(key);
            } else {
                //@TODO desconsiderar @TRansiente
                path = path.get(nestedField);
            }

            // Adiciona um ponto final ao joinKey se não for o último elemento
            if (i < nestedFields.length - 1) {
                joinKey.append(".");
            }
        }

        return path;
    }

    public PageImpl<Map<String, Object>> queryEntitiesPaged(Class<?> entityClass, Map<String, Field[]> fieldsMap, int page, int size, boolean grouped, String filter, String sort) {
        final List<String> fields = new ArrayList<>(fieldsMap.keySet());//temporario

        long totalElements = queryCount(entityClass, filter);
        List<Object[]> resultList = queryEntities(entityClass, fields, null, page, size, filter, sort);
        List<Map<String, Object>> content = ResultProcessor.consolidateResults(entityClass, resultList, fieldsMap);
        return new PageImpl<>(content, size, page, totalElements);
    }

    /**
     * Cria a consulta usando a Criteria API para buscar as entidades. Se nenhum campo for
     * especificado, todos os campos da entidade são retornados.
     *
     * @param entityClass
     * @param fields
     * @param id
     * @param page
     * @param size
     * @param filter
     * @param sort
     * @return
     */
    public List<Object[]> queryEntities(Class<?> entityClass, List<String> fields, Object id, int page, int size, String filter, String sort) {
        Throw.ifIsTrue(fields == null || fields.isEmpty(), "fields");

        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Object[]> criteriaQuery = criteriaBuilder.createQuery(Object[].class);
        Root<?> root = criteriaQuery.from(entityClass);

        List<Selection<?>> selections = createSelections(root, fields);

        criteriaQuery.multiselect(selections);
        criteriaQuery.distinct(true);//@todo se tiver colletions em fields

        if (id != null) {
            criteriaQuery.where(criteriaBuilder.equal(root.get("id"), id)); ///@TODO nome do campo Id
        }

        if (filter != null && !filter.isEmpty()) {
            criteriaQuery.where(new SearchQueryCriteriaConsumer(criteriaBuilder, root, filter).getPredicate());
        }

        // SORT para paginacao é importante ter um order by unico
        String idFieldName = PropertyId.getIdFieldName(entityClass);

        if (sort == null) {
            sort = "";
        }

        sort = sort.trim();

        if (sort.isEmpty()) {
            sort = idFieldName;
        } else {
            sort += "," + idFieldName;
        }

        criteriaQuery.orderBy(createSortOrder(criteriaBuilder, root, sort));

        // executa query
        Query query = getEntityManager().createQuery(criteriaQuery);

        if (page >= 0) {
            query.setFirstResult(page * size);
        }

        if (size > 0) {
            query.setMaxResults(size);
        }

        List<Object[]> resultList = query.getResultList();
        return resultList;
    }

    /**
     * Verifica se um campo é uma associação.
     *
     * @param root
     * @param fieldName
     * @return
     */
    private boolean isAssociation(Path<?> root, String fieldName) {
        try {
            Field field = root.getJavaType().getDeclaredField(fieldName);
            return field.isAnnotationPresent(OneToOne.class)
                    || field.isAnnotationPresent(OneToMany.class)
                    || field.isAnnotationPresent(ManyToOne.class)
                    || field.isAnnotationPresent(ManyToMany.class)
                    || field.isAnnotationPresent(Embedded.class);
        } catch (NoSuchFieldException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Cria uma lista de ordenações com base nos parâmetros fornecidos.
     *
     * @param cb
     * @param root
     * @param sort
     * @return
     */
    private List<Order> createSortOrder(CriteriaBuilder cb, Root<?> root, String sort) {
        List<Order> orders = new ArrayList<>();
        String[] fields = sort.split(",");
        for (String field : fields) {
            boolean ascending = true;
            if (field.startsWith("-")) {
                ascending = false;
                field = field.substring(1);
            }
            orders.add(ascending ? cb.asc(getPath(root, field)) : cb.desc(getPath(root, field)));
        }
        return orders;
    }

    private Path<?> getPath(Root<?> root, String field) {
        String[] parts = field.split("\\.");
        Path<?> path = root;
        for (String part : parts) {
            path = path.get(part);
        }
        return path;
    }

    /**
     * Obtém o número total de elementos que correspondem aos critérios.
     *
     * @param entityClass
     * @param filter
     * @return
     */
    private long queryCount(Class<?> entityClass, String filter) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<?> root = criteriaQuery.from(entityClass);
        criteriaQuery.select(criteriaBuilder.count(root));
        if (filter != null && !filter.isEmpty()) {
            criteriaQuery.where(new SearchQueryCriteriaConsumer(criteriaBuilder, root, filter).getPredicate());
        }
        return getEntityManager().createQuery(criteriaQuery).getSingleResult();
    }

}
