package javax.util.spring.noap.restfull.exceptionhandler;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * <pre>
 *   ExceptionChain exceptionChain = new ExceptionChain();
 *   ProblemDetail problemDetail = exceptionChain.handle(exception);
 * </pre>
 *
 * @author Marcius Brandão
 */
public class HandlerExceptionChain {

    private final List<HandlerException> handlers;

    public HandlerExceptionChain() {
        handlers = Arrays.asList(
                new HandlerClassNotFoundException(),
                new HandlerConstraintViolationException(),
                new HandlerEntityExistsException(),
                new HandlerEntityNotFoundException(),
                new HandlerNoSuchExceptions(),
                new HandlerSQLException(),
                new HandlerIllegalArgumentException()
        // adicionar demais handlers aqui se necessario
        );
    }

    public ProblemDetail handle(Throwable exception) {
        exception.printStackTrace();
        
        Throwable exceptionCause = getRootCause(exception);

        for (HandlerException handler : handlers) {
            if (handler.canHandle(exceptionCause)) {
                return handler.handle(exceptionCause);
            }
        }

         // Default handler
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        String title = "Ocorreu um erro inesperado";
        LocalDateTime now = LocalDateTime.now();

        ResponseStatus statusAnnotation = exceptionCause.getClass().getAnnotation(ResponseStatus.class);
        if (statusAnnotation != null) {
            httpStatus = statusAnnotation.value();
            title = statusAnnotation.reason();
            now = null;
        }
       
        return ProblemDetail
                .builder()
                .status(httpStatus.value())
                .title(title)
                .detail(exceptionCause.getMessage())
                .details(Arrays.asList(exception.getMessage().split("\\n")))
                .timestamp(now)
                .build();
    }

    // Método para obter a exceção raiz
    private Throwable getRootCause(Throwable ex) {//@TODO está perdendo as mensgens da pilha
        Throwable rootCause = ex;
        while (rootCause.getCause() != null && rootCause.getCause() != rootCause) {
            rootCause = rootCause.getCause();
        }
        return rootCause;
    }
}
