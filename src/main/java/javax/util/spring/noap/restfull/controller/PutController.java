package javax.util.spring.noap.restfull.controller;

import javax.util.spring.noap.restfull.controller.BaseController;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.NoSuchElementException;
import javax.util.ddd.infra.util.CaseUtils;
import javax.util.spring.noap.core.Reflections;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller ainda em desenvolvimento
 *
 * @author Marcius
 */
@RestController
@RequestMapping(value = "/domain")
public class PutController extends BaseController {

    public record MethodInvocationRequest(Map<String, Object> set, Map<String, Object> args) {

    }

    @PutMapping("/{entityName}/{id}/{methodName}")
    public ResponseEntity<Object> invokeEntityMethod(
            @PathVariable(name = "entityName", required = true) String entityName,
            @PathVariable(name = "id", required = true) String id,
            @PathVariable(name = "methodName", required = true) String methodName,
            @RequestBody(required = true) MethodInvocationRequest request) throws Exception {

//        String entitySimpleName = CaseUtils.kebabCaseToCamelCase(entityName);
//        String entityMethodName = CaseUtils.kebabCaseToCamelCaseNoCapitalizeFirstLetter(methodName);
//
//        Class<?> entityClass = getEntityFinder().findEntityClass(entitySimpleName)
//                .orElseThrow(() -> new NoSuchElementException(entitySimpleName));
//
//        Object entity = getRepository()
//                .get(entityClass, id)
//                .orElseThrow(() -> new NoSuchElementException(entityClass.getSimpleName() + " (" + id + ")"));
//
//        // Set properties
//        for (Map.Entry<String, Object> entry : request.set().entrySet()) {
//            PropertySetter.setNestedProperty(entity, entry.getKey(), entry.getValue());
//        }
//
//        // Prepare and invoke the method
//        Method method = Reflections.findMethodWithParams(entityClass, entityMethodName, request.args())
//                .orElseThrow(() -> new NoSuchMethodException("Operação " + entityMethodName + "() não encontrada na entidade " + entitySimpleName));
//
//        Object[] args = request.args().values().toArray();
//        Object result = method.invoke(entity, args);
//
//        getRepository().set(entity);
//
//        // Check if the method type is void
//        if (method.getReturnType().equals(Void.TYPE)) {
//            return ResponseEntity.noContent().build();
//        } else {
//            return ResponseEntity.ok(Map.of("return", result));
//        }
        throw new UnsupportedOperationException("Ainda não implementado");
    }
}
