package javax.util.spring.noap.restfull.controller;

import javax.util.spring.noap.restfull.repository.JPACriteriaRepository;
import jakarta.persistence.EntityNotFoundException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;
import javax.util.ddd.infra.PageImpl;
import javax.util.spring.noap.core.FieldExtractor;
import javax.util.spring.noap.core.ResultProcessor;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author Marcius Brandão
 *
 */
@RestController
@RequestMapping(value = "/domain")
public class GetController extends JPACriteriaRepository {

    @GetMapping("/{entityName}/all")
    public ResponseEntity<?> getAll(
            @PathVariable(name = "entityName", required = true) String entityName,
            @RequestParam(name = "fields", required = false, defaultValue = "") List<String> fields,
            @RequestParam(name = "filter", required = false) String filter,
            @RequestParam(name = "sort", required = false) String sort) throws ClassNotFoundException {

        Class<?> entityClass = findEntityClassOrThrow(entityName);

        Map<String, Field[]> fieldsMap = FieldExtractor.extractFields(entityClass, fields);
        final List<String> finalFields = new ArrayList<>(fieldsMap.keySet());

        List<Object[]> resultList = queryEntities(entityClass, finalFields, null, -1, -1, filter, sort);
        List<Map<String, Object>> content = ResultProcessor.consolidateResults(entityClass, resultList, fieldsMap);
        return ResponseEntity.ok(content);
    }

    @GetMapping("/{entityName}")
    public ResponseEntity<?> getAllPaged(
            @PathVariable(name = "entityName", required = true) String entityName,
            @RequestParam(name = "fields", required = false, defaultValue = "") List<String> fields,
            @RequestParam(name = "filter", required = false) String filter,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size,
            @RequestParam(name = "sort", required = false) String sort,
            @RequestParam(name = "grouped", defaultValue = "true") boolean grouped) throws ClassNotFoundException {

        Class<?> entityClass = findEntityClassOrThrow(entityName);

        Map<String, Field[]> fieldsMap = FieldExtractor.extractFields(entityClass, fields);

        PageImpl<Map<String, Object>> results = queryEntitiesPaged(entityClass, fieldsMap, page, size, grouped, filter, sort);
        
        return ResponseEntity.status(206).body(results);
    }

    @GetMapping("/{entityName}/{id}")
    public ResponseEntity<?> getEntityById(
            @PathVariable(name = "entityName", required = true) String entityName,
            @PathVariable(name = "id", required = true) Object id,
            @RequestParam(name = "fields", required = false, defaultValue = "") List<String> fields) throws ClassNotFoundException {

        Class<?> entityClass = findEntityClassOrThrow(entityName);

        Map<String, Field[]> fieldsMap = FieldExtractor.extractFields(entityClass, fields);
        final List<String> finalFields = new ArrayList<>(fieldsMap.keySet());

        // Use o TransactionTemplate para iniciar uma transação
        return getTransactionTemplate().execute(status -> {
            try {
                List<Object[]> resultList = queryEntities(entityClass, finalFields, id, -1, -1, null, null);
                if (resultList.isEmpty()) {
                    throw new EntityNotFoundException(entityClass.getSimpleName() + " (" + id + ")");
                }

                List<Map<String, Object>> content = ResultProcessor.consolidateResults(entityClass, resultList, fieldsMap);
                return ResponseEntity.ok(content.get(0));
            } catch (EntityNotFoundException e) {
                // Se ocorrer algum erro, você pode fazer rollback da transação aqui
                status.setRollbackOnly();
                throw new RuntimeException(e);
            }
        });
    }

}
