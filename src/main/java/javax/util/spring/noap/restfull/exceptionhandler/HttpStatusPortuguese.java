package javax.util.spring.noap.restfull.exceptionhandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;

public enum HttpStatusPortuguese {
    OK(200, "OK"),
    CREATED(201, "Criado"),
    NO_CONTENT(204, "Sem Conteúdo"),
    BAD_REQUEST(400, "Requisição Inválida"),
    UNAUTHORIZED(401, "Não Autorizado"),
    FORBIDDEN(403, "Acesso Negado"),
    NOT_FOUND(404, "Não Encontrado"),
    UNPROCESSABLE_ENTITY(422, "Já cadastrado"),
    INTERNAL_SERVER_ERROR(500, "Erro Interno do Servidor");
    // @TODO Adicionar mais status conforme necessário
    //CONFLICT

    private final int code;
    private final String reasonPhrase;

    HttpStatusPortuguese(int code, String reasonPhrase) {
        this.code = code;
        this.reasonPhrase = reasonPhrase;
    }

    public int getCode() {
        return code;
    }

    public String getReasonPhrase() {
        return reasonPhrase;
    }

    public static String getReasonPhrase(HttpStatusCode httpStatusCode) {
        return getReasonPhraseByCode(httpStatusCode.value());
    }
    
    public static String getReasonPhraseByCode(int code) {
        for (HttpStatusPortuguese status : values()) {
            if (status.getCode() == code) {
                return status.getReasonPhrase();
            }
        }
        return HttpStatus.valueOf(code).getReasonPhrase();
    }
}

