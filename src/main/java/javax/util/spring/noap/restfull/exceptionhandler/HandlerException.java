package javax.util.spring.noap.restfull.exceptionhandler;

public interface HandlerException {

    boolean canHandle(Throwable exception);

    ProblemDetail handle(Throwable exception);
}
