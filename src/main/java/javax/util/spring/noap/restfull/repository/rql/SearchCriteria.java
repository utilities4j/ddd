package javax.util.spring.noap.restfull.repository.rql;

import static javax.util.spring.noap.restfull.repository.rql.SearchOperation.*;
import java.util.Objects;

public class SearchCriteria {

    private final String key;
    private final SearchOperation operation;
    private final Object value;
    private final Boolean orPredicate;

    // Construtor privado para forçar o uso do Builder
    private SearchCriteria(Builder builder) {
        this.key = builder.key;
        this.operation = builder.operation;
        this.value = builder.value;
        this.orPredicate = builder.orPredicate;
    }

    // Getters
    public String getKey() {
        return key;
    }

    public SearchOperation getOperation() {
        return operation;
    }

    public Object getValue() {
        return value;
    }

    public Boolean isOrPredicate() {
        return orPredicate != null && orPredicate;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.key);
        hash = 31 * hash + Objects.hashCode(this.operation);
        hash = 31 * hash + Objects.hashCode(this.value);
        hash = 31 * hash + (this.orPredicate ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SearchCriteria other = (SearchCriteria) obj;
        if (!Objects.equals(this.orPredicate, other.orPredicate)) {
            return false;
        }
        if (!Objects.equals(this.key, other.key)) {
            return false;
        }
        if (this.operation != other.operation) {
            return false;
        }
        return Objects.equals(this.value, other.value);
    }

    @Override
    public String toString() {
        if (orPredicate != null) {
            return (orPredicate ? ") OR (" : " AND ") + key + operation + "'" + value + "'";
        }
        return key + operation + "'" + value + "'";
    }
   
    // Classe Builder
    public static class Builder {

        private String key;
        private SearchOperation operation;
        private Object value;
        private Boolean orPredicate;

        public Builder() {
        }

        public Builder key(String key) {
            this.key = key;
            return this;
        }

        public Builder operation(String operation) {
            this.operation = SearchOperation.getSimpleOperation(operation);
            return this;
        }

        public Builder value(Object value) {
            this.value = value;
            return this;
        }

        public Builder orPredicate(String orPredicate) {
            if (orPredicate == null || orPredicate.isEmpty() || orPredicate.isBlank()) {
                this.orPredicate = null;
            } else if (orPredicate.trim().equals(OR_PREDICATE_FLAG)) {
                this.orPredicate = true;
            } else if (orPredicate.trim().equals(AND_PREDICATE_FLAG)) {
                this.orPredicate = false;
            } else {
                throw new IllegalArgumentException(orPredicate);
            }

            return this;
        }

        public SearchCriteria build() {
            SearchCriteria sc = new SearchCriteria(this);
            System.out.println(sc.toString());
            return sc;
        }
    }
}
