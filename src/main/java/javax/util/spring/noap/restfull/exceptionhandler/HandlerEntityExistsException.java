package javax.util.spring.noap.restfull.exceptionhandler;

import jakarta.persistence.EntityExistsException;
import org.springframework.http.HttpStatus;

public class HandlerEntityExistsException implements HandlerException {

    @Override
    public boolean canHandle(Throwable exception) {
        return exception instanceof EntityExistsException;
    }

    @Override
    public ProblemDetail handle(Throwable exception) {
        return ProblemDetail
                .builder()
                .status(HttpStatus.CONFLICT.value())
                .title("Informação já cadastrada")
                .detail(exception.getMessage())
                .build();
    }
}
