package javax.util.spring.noap.restfull.repository.rql;

import java.util.HashMap;
import java.util.Map;

/*
 * We have two sets of operations:
 * 1. Simple – can be represented by one character:
 *    Equality: represented by colon (:) 
 *    Negation: represented by Exclamation mark (!)
 *    Greater than: represented by (>)
 *    Less than: represented by (<)
 *    Like: represented by tilde (~)

 * 2. Complex – need more than one character to be represented:
 *    Starts with: represented by (=prefix*)
 *    Ends with: represented by (=*suffix)
 *    Contains: represented by (=*substring*)
 *  
 */
public enum SearchOperation {
    EQUALITY {
        @Override
        public String toString() {
            return " = ";
        }
    },
    NEGATION {
        @Override
        public String toString() {
            return " <> ";
        }
    },
    GREATER_THAN {
        @Override
        public String toString() {
            return " > ";
        }
    },
    LESS_THAN {
        @Override
        public String toString() {
            return " < ";
        }
    },
    LIKE {
        @Override
        public String toString() {
            return " iLike ";
        }
    },
    GREATER_THAN_OR_EQUAL {
        @Override
        public String toString() {
            return " >= ";
        }
    },
    LESS_THAN_OR_EQUAL {
        @Override
        public String toString() {
            return " <= ";
        }
    };

    private static final Map<String, SearchOperation> operationMap = new HashMap<>() {
        {
            put("eq", SearchOperation.EQUALITY);
            put(":", SearchOperation.EQUALITY);
            put("ne", SearchOperation.NEGATION);
            put("gt", SearchOperation.GREATER_THAN);
            put(">", SearchOperation.GREATER_THAN);
            put("lt", SearchOperation.LESS_THAN);
            put("<", SearchOperation.LESS_THAN);
            put("~", SearchOperation.LIKE);
            put("like", SearchOperation.LIKE);
            put("ge", SearchOperation.GREATER_THAN_OR_EQUAL);
            put(">:", SearchOperation.GREATER_THAN_OR_EQUAL);
            put("le", SearchOperation.LESS_THAN_OR_EQUAL);
            put("<:", SearchOperation.LESS_THAN_OR_EQUAL);
        }
    };

    public static final String OR_PREDICATE_FLAG = "or";
    public static final String AND_PREDICATE_FLAG = "and";

    public static SearchOperation getSimpleOperation(final String input) {
        SearchOperation operation = operationMap.get(input);

        if (operation == null) {
            throw new IllegalArgumentException(input);
        } else {
            return operation;
        }
    }

}
