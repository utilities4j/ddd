package javax.util.spring.noap.restfull.exceptionhandler;

import java.time.LocalDateTime;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 *
 *
 * @author Marcius
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private static final HttpHeaders HEADERS_PROBLEM = new HttpHeaders() {
        {
            add("Content-Type", "application/problem+json");
        }
    };

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatusCode statusCode, WebRequest request) {
        if (body == null) {
            body = ProblemDetail
                    .builder()
                    .status(statusCode.value())
                    .detail(ex.getMessage())
                    .build();
        } else if (body instanceof String string) {
            body = ProblemDetail
                    .builder()
                    .status(statusCode.value())
                    .detail(string)
                    .build();
        }
        return super.handleExceptionInternal(ex, body, headers, statusCode, request);
    }

    /**
     * Customizar outras exceptions do ResponseEntityExceptionHandler
     *
     * @param ex
     * @param headers
     * @param status
     * @param request
     * @return
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {

        ProblemDetail problem = ProblemDetail
                .builder()
                .status(status.value())                
                .detail("in " + request.getDescription(false) + ": " + ex.getMessage())
                .timestamp(LocalDateTime.now())
                .build();

        return super.handleExceptionInternal(ex, problem, headers, status, request);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleInternalServerErrorException(Exception ex, WebRequest webRequest) {
        HandlerExceptionChain exceptionChain = new HandlerExceptionChain();
        ProblemDetail problemDetail = exceptionChain.handle(ex);
        
        return handleExceptionInternal(ex, problemDetail, HEADERS_PROBLEM, problemDetail.getHttpStatusCode(), webRequest);
    }
}
