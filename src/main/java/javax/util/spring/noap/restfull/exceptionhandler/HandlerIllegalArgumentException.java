package javax.util.spring.noap.restfull.exceptionhandler;

import java.util.Arrays;
import org.springframework.http.HttpStatus;

public class HandlerIllegalArgumentException implements HandlerException {

    @Override
    public boolean canHandle(Throwable exception) {
        return exception instanceof IllegalArgumentException;
    }

    @Override
    public ProblemDetail handle(Throwable exception) {
        return ProblemDetail
                .builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .title("Dados inválidos")
                .details(Arrays.asList(exception.getMessage().split("\\n")))
                .build();
    }

}
