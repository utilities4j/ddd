package javax.util.spring.noap.restfull.exceptionhandler;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.http.HttpStatus;

public class HandlerEntityNotFoundException implements HandlerException {

    @Override
    public boolean canHandle(Throwable exception) {
        return exception instanceof EntityNotFoundException;
    }

    @Override
    public ProblemDetail handle(Throwable exception) {
        return ProblemDetail
                .builder()
                .status(HttpStatus.NOT_FOUND.value())
                .title("Informação não encontrada")
                .detail(exception.getMessage())
                .build();
    }
}
