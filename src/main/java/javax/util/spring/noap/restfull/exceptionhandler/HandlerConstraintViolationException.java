package javax.util.spring.noap.restfull.exceptionhandler;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.springframework.http.HttpStatus;

public class HandlerConstraintViolationException implements HandlerException {

    @Override
    public boolean canHandle(Throwable exception) {
        return exception instanceof ConstraintViolationException;
    }

    @Override
    public ProblemDetail handle(Throwable exception) {
        ConstraintViolationException ex = (ConstraintViolationException) exception;
        List<String> errors = new ArrayList<>();
        Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
        if (!violations.isEmpty()) {
            for (ConstraintViolation violation : violations) {
                errors.add(violation.getPropertyPath().toString()
                        .concat(": ")
                        .concat(violation.getMessage()));
            }
        }

        HttpStatus status = HttpStatus.BAD_REQUEST;

        return ProblemDetail
                .builder()
                .status(status.value())
                .title("Dados inválidos")
                .details(errors)
                .build();
    }
}
