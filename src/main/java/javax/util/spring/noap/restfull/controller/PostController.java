package javax.util.spring.noap.restfull.controller;

import jakarta.persistence.EntityExistsException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.util.spring.noap.core.PropertyId;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.CREATED;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.support.TransactionTemplate;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller ainda em desenvolvimento
 *
 * @author Marcius
 */
@RestController
@RequestMapping(value = "/domain")
public class PostController extends BaseController {

    @PostMapping("/{entityName}/new")
    public ResponseEntity<Object> createNewEntityInstance(
            @PathVariable(name = "entityName", required = true) String entityName) throws Exception {

        Class<?> entityClass = findEntityClassOrThrow(entityName);
        Object newInstance = entityClass.getDeclaredConstructor().newInstance();
        return ResponseEntity.ok(newInstance);
    }

    @Autowired
    private TransactionTemplate transactionTemplate;

    @PostMapping("/{entityName}")
    public ResponseEntity<?> insert(
            @PathVariable(name = "entityName", required = true) String entityName,
            @RequestBody(required = true) String jsonString,
            @RequestParam(name = "fields", required = false) List<String> fields) throws EntityExistsException, ClassNotFoundException, Exception  {

        Map<String, Object> responseMap = new LinkedHashMap<>();        
        Class<?> entityClass = findEntityClassOrThrow(entityName);

        transactionTemplate.execute(status -> {
            try {                
                Object entity = entityClass.getDeclaredConstructor().newInstance();
                jsonToObject(jsonString, entity);
                
                throwIfEntityExists(entity);                
                validate(entity);

                getEntityManager().persist(entity);

                PropertyId.IdProperty idField = PropertyId.getIdProperty(entity);
                responseMap.put(idField.name(), idField.value());

                if (fields != null && !fields.isEmpty()) {
                    Object updatedEntity = getEntityManager().find(entityClass, idField.value());//@TODO otimzar trazendo apenas os campos solicitados
                    responseMap.putAll(getSelectedFields(updatedEntity, fields));
                }
            } catch (Exception ex) {
                status.setRollbackOnly();
                throw new RuntimeException(ex);
            } 
            return null;
        });

        return ResponseEntity.status(CREATED).body(responseMap);
    }
   

}
