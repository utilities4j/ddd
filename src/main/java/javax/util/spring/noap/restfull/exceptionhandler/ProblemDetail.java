package javax.util.spring.noap.restfull.exceptionhandler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.net.URI;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;

/**
 * Implementação do padrão RFC 7807
 *
 * Devem ser usados os códigos de Status HTTP entre os ranges 400 e 500 para representar mensagens
 * de erro.
 *
 * O header Content-Type deve ser do tipo application/problem, incluindo o formato de serialização
 * da mensagem. Ex: application/problem+json
 *
 * exemplo:
 * <pre>
 * ProblemDetail problemDetail = ProblemDetail.builder()
 *   .type("about:blank")
 *   .status(404)  // Isso definirá automaticamente o títle para "Não Encontrado"
 *   .detail("O recurso que você procurava não foi encontrado.")
 *   .addDetails("Observação 1")
 *   .addDetails("Observação 2")
 *   .build();
 * </pre>
 *
 * @version 1.0
 * @author Marcius
 * @see https://www.rfc-editor.org/rfc/rfc9457
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProblemDetail {

    /**
     * Uma URL para um documento que descreva o tipo do problema;
     */
    private URI type;

    /**
     * used for the convenience of the consumer. O status HTTP gerado pelo servidor de origem.
     * Normalmente deve ser o mesmo status HTTP da resposta, e pode servir de referência para casos
     * onde um servidor proxy altera o status da resposta;
     */
    private Integer status;

    /**
     * Um breve resumo do tipo de problema LEGÍVEL PARA HUMANOS. Não deve mudar para ocorrências do
     * mesmo tipo, exceto para fins de localização;
     */
    private String title;

    /**
     * Descrição detalhada do problema e, se possível, sugerir como resolver O membro "detail" é uma
     * cadeia de caracteres JSON que contém uma explicação legível por humanos específica para essa
     * ocorrência do problema.
     *
     * A cadeia de caracteres "detail", se presente, deve se concentrar em ajudar o cliente a
     * corrigir o problema, em vez de fornecer informações de depuração.
     *
     * Consumidores NÃO DEVERIA analisar o membro "detalhe" para obter informações; As extensões são
     * formas mais adequadas e menos propensas a erros para obter essas informações.
     *
     */
    private String detail;

    private LocalDateTime timestamp;

    private List<String> details;

    //<editor-fold defaultstate="collapsed" desc="Getters and Setters">
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
        //Garante que o título esteja definido corretamente
        this.title = HttpStatusPortuguese.getReasonPhraseByCode(status);
    }

    public URI getType() {
        return type;
    }

    public void setType(URI type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public List<String> getDetails() {
        if (details == null) {
            details = new ArrayList<>();
        }
        return details;
    }

    public void setDetails(List<String> details) {
        this.details = details;
    }

    @JsonIgnore
    public HttpStatusCode getHttpStatusCode() {
        return HttpStatusCode.valueOf(getStatus());
    }
    //</editor-fold>

    // Static method to start the building process
    static Builder builder() {
        return new Builder();
    }

    void setStatus(HttpStatus httpStatus) {
        setStatus(httpStatus.value());
    }

    // Builder pattern implementation
    public static class Builder {

        private URI type;
        private Integer status;
        private String title;
        private String detail;
        private LocalDateTime timestamp;
        private List<String> details;

        public Builder type(URI type) {
            this.type = type;
            return this;
        }

        public Builder status(Integer status) {
            this.status = status;
            //Garante que o título esteja definido corretamente
            this.title = HttpStatusPortuguese.getReasonPhraseByCode(status);
            return this;
        }

        public Builder title(String title) {
            if (title != null && !title.isBlank()) {
                this.title = title;
            }
            return this;
        }

        public Builder detail(String detail) {
            this.detail = detail;
            return this;
        }

        public Builder timestamp(LocalDateTime timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder details(List<String> details) {
            this.details = details;
            return this;
        }

        public Builder addDetails(String detail) {
            if (detail != null && !detail.trim().isEmpty()) {
                if (details == null) {
                    details = new ArrayList<>();
                }
                details.add(detail);
            }
            return this;
        }

        public ProblemDetail build() {
            ProblemDetail problemDetail = new ProblemDetail();
            problemDetail.setType(this.type);
            problemDetail.setStatus(this.status);
            problemDetail.setTitle(this.title);
            problemDetail.setDetail(this.detail);
            problemDetail.setTimestamp(this.timestamp);
            problemDetail.setDetails(this.details);
            return problemDetail;
        }
    }
}
