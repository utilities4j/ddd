package javax.util.spring.noap.restfull.controller;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Marcius Brandão
 *
 * @version 1.0
 */
@RestController
@RequestMapping(value = "/domain")
public class DeleteController extends BaseController {

    @DeleteMapping(value = "/{entityName}/{id}")
    public ResponseEntity<?> remove(
            @PathVariable(name = "entityName", required = true) String entityName,
            @PathVariable(name = "id", required = true) Object id) throws Exception {

        Class<?> entityClass = findEntityClassOrThrow(entityName);

        getTransactionTemplate().execute(status -> {
            try {
                deleteEntityById(entityClass, id);
            } catch (Exception ex) {
                status.setRollbackOnly();
                throw new RuntimeException(ex);
            }
            return null;
        });
        return ResponseEntity.noContent().build();
    }

    private void deleteEntityById(Class<?> entityClass, Object id) {
        Object entity = getEntityManager().find(entityClass, id);
        if (entity != null) {
            getEntityManager().remove(entity);
        } else {
            throw new EntityNotFoundException(entityClass.getSimpleName() + " (" + id + ")");
        }
    }

}
