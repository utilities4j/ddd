package javax.util.spring.noap.restfull.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.PersistenceContext;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.util.ddd.infra.jpa.metamodel.EntityFinder;
import javax.util.ddd.infra.util.CaseUtils;
import javax.util.spring.noap.core.PropertyId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Controller ainda em desenvolvimento
 *
 * @author Marcius
 */
public class BaseController {

    private EntityFinder entityFinder;

    @Autowired
    private Validator validator;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private TransactionTemplate transactionTemplate;

    public BaseController() {
    }

    @PostConstruct
    public void initialize() {
        entityFinder = new EntityFinder(entityManager);
    }

    public void validate(Object object) {
        Set<ConstraintViolation<Object>> violations = validator.validate(object);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }

    public EntityFinder getEntityFinder() {
        return entityFinder;
    }

    public Validator getValidator() {
        return validator;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public TransactionTemplate getTransactionTemplate() {
        return transactionTemplate;
    }

    public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
    }

    public Class<?> findEntityClassOrThrow(String entityNameKebabCase) throws ClassNotFoundException {
        String entityName = CaseUtils.kebabCaseToCamelCase(entityNameKebabCase);
        return entityFinder.findEntityClass(entityName)
                .orElseThrow(() -> new ClassNotFoundException(entityName));
    }

    public Object findEntityInstanceOrThrow(Class<?> entityClass, Object entityId) {
        Object entity = getEntityManager().find(entityClass, entityId);
        if (entity == null) {
            throw new EntityNotFoundException(entityClass.getSimpleName() + " (" + entityId + ")");
        }
        return entity;
    }

    public void throwIfEntityNotExist(Class<?> entityClass, Object id) {
        if (!entityExists(entityClass, id)) {
            throw new EntityNotFoundException(entityClass.getSimpleName() + " (" + id + ")");
        }
    }

    public void throwIfEntityExists(Class<?> entityClass, Object id) {
        if (entityExists(entityClass, id)) {
            throw new EntityExistsException(entityClass.getSimpleName() + " (" + id + ")");
        }
    }

    public void throwIfEntityExists(Object entity) throws Exception {
        Object id = PropertyId.getIdFieldValue(entity);
        if (entityExists(entity.getClass(), id)) {
            throw new EntityExistsException(entity.getClass().getSimpleName() + " (" + id + ")");
        }
    }

    private boolean entityExists(Class<?> entityClass, Object id) {
        String idFieldName = PropertyId.getIdFieldName(entityClass);
        String jpql = String.format(
                "SELECT COUNT(e) FROM %s e WHERE e.%s = :id",
                entityClass.getSimpleName(),
                idFieldName);

        Long count = getEntityManager().createQuery(jpql, Long.class)
                .setParameter("id", id)
                .getSingleResult();
        return count > 0;
    }

    @Autowired
    private ObjectMapper objectMapper;

    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

//    public ObjectMapper getObjectMapper() {
//        if (objectMapper == null) {
//            objectMapper = new ObjectMapper();
//
//            objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
//            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//            objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
//            // Habilita formatação bonita (pretty-print) para saída JSON
//            objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
//            objectMapper.registerModule(new JavaTimeModule());
//            //objectMapper.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));
//
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
//            SimpleModule module = new SimpleModule();
//            module.addSerializer(Date.class, new DateSerializer(false, dateFormat));
//            objectMapper.registerModule(module);
//        }
//
//        return objectMapper;
//    }

    public void jsonToObject(String jsonString, Object target) {
        try {
            getObjectMapper().readerForUpdating(target).readValue(jsonString);
        } catch (JsonProcessingException ex) {
            throw new RuntimeException(jsonString, ex);
        }
    }

    public Map<String, Object> getSelectedFields(Object entity, List<String> fields) throws Exception {
        Map<String, Object> selectedFields = new LinkedHashMap<>();
        for (String fieldName : fields) {
            Field field = entity.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            selectedFields.put(fieldName, field.get(entity));
        }
        return selectedFields;
    }

}
