package javax.util.spring.noap.core;

import jakarta.persistence.Embedded;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import static javax.util.spring.noap.core.Reflections.isCollection;
import org.apache.commons.lang3.reflect.FieldUtils;

public class FieldExtractor {

    public static Map<String, Field[]> extractFields(Class<?> entityRoot, List<String> fieldsNames) {
        Map<String, Field[]> result = new LinkedHashMap<>();
        
        //Primeira coluna deve ser o @Id
        String fieldIdName = PropertyId.getIdFieldName(entityRoot);
        Field[] aFieldsId = FieldFinderNested.findAllFields(entityRoot, fieldIdName);
        result.put(fieldIdName, aFieldsId);

        if (fieldsNames.isEmpty()) {
            return getAllFieldsWithGetters(entityRoot, entityRoot, "", new ArrayList<>(), new LinkedHashMap<>());
        }

        if (fieldsNames.get(0).equals("*")) {
            fieldsNames.remove("*");
            result.putAll(getAllFieldsWithGetters(entityRoot, entityRoot, "", new ArrayList<>(), new LinkedHashMap<>()));
        }

        String fieldNameBefore = "";        
        for (String fieldName : fieldsNames) {
            if (fieldName.startsWith("*.")) {
                fieldName = fieldName.replaceFirst("\\*", fieldNameBefore);
                fieldNameBefore = fieldName;
            } else if (fieldName.startsWith("$.")) {
                fieldName = fieldName.replaceFirst("\\$", fieldNameBefore);
            } else {
                fieldNameBefore = fieldName;
            }

            Field[] aFields = FieldFinderNested.findAllFields(entityRoot, fieldName);
            if (aFields == null || aFields.length == 0) {
                throw new NoSuchFieldError(fieldName);
            }

            Field field = aFields[aFields.length - 1]; // Checking the deepest field for association

            if (field.isAnnotationPresent(Embedded.class)) {
                result.putAll(getAllFieldsWithGetters(entityRoot, field.getType(), fieldName + ".", new ArrayList<>(), new LinkedHashMap<>()));
            } else if (Reflections.isCollection(field)) {
                result.putAll(getAllFieldsWithGetters(entityRoot, getGenericType(field), fieldName + ".", new ArrayList<>(), new LinkedHashMap<>()));
            } else if (Reflections.isAssociation(field)) {
                result.putAll(getAllFieldsWithGetters(entityRoot, field.getType(), fieldName + ".", new ArrayList<>(), new LinkedHashMap<>()));
            } else {
                result.put(fieldName, aFields);
            }
        }

        return result;
    }

    //@TODO metodo repetido
    private static Class<?> getGenericType(Field field) {
        try {
            return (Class<?>) ((java.lang.reflect.ParameterizedType) field.getGenericType())
                    .getActualTypeArguments()[0];
        } catch (Exception e) {
            return Object.class; // Fallback para Object se não for possível determinar o tipo genérico
        }
    }
    
    private static boolean hasGetter(Class<?> clazz, Field field) {
        String getterName = "get" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
        try {
            Method method = clazz.getMethod(getterName);
            return method != null;
        } catch (NoSuchMethodException e) {
            return false;
        }
    }

    private static Map<String, Field[]> getAllFieldsWithGetters(Class<?> root, Class<?> clazz, String prefix, List<Class<?>> visitedClasses, Map<String, Field[]> result) {
        if (visitedClasses.contains(clazz)) {
            return result;
        }

        visitedClasses.add(clazz);

        for (Field field : FieldUtils.getAllFields(clazz)) {
            if (!isCollection(field) && hasGetter(clazz, field)) {
                Field[] afields = FieldFinderNested.findAllFields(root, prefix + field.getName());
                if (afields != null && afields.length > 0) {//@todo se não achar, lancar erro?
                    Field afield = afields[afields.length - 1]; // Checking the deepest field for association
                    if (afield.isAnnotationPresent(Embedded.class)) {
                        result.putAll(getAllFieldsWithGetters(root, afield.getType(), prefix + field.getName() + ".", visitedClasses, new LinkedHashMap<>()));
                    } else if (Reflections.isAssociation(afield)) {
                        //evitar o overfetch e cicly
                        //result.putAll(getAllFieldsWithGetters(root, afield.getType(), prefix + field.getName() + ".", visitedClasses, new LinkedHashMap<>()));
                    } else {
                        result.put(prefix + field.getName(), afields);
                    }
                }
            }
        }

        visitedClasses.remove(clazz);

        return result;
    }

    public static void main(String[] args) {
        class Cidade {

            private Long id;
            private String nome;

            public Long getId() {
                return id;
            }

            public void setId(Long id) {
                this.id = id;
            }

            public String getNome() {
                return nome;
            }

            public void setNome(String nome) {
                this.nome = nome;
            }

        }
        // Teste
        class Endereco {

            private String rua;

            @ManyToOne
            private Cidade cidade;

            public String getRua() {
                return rua;
            }

            public void setRua(String rua) {
                this.rua = rua;
            }

            public Cidade getCidade() {
                return cidade;
            }

            public void setCidade(Cidade cidade) {
                this.cidade = cidade;
            }

        }

        class Usuario {

            private Long id;
            private String nome;
            @Embedded
            private Endereco endereco;

            @OneToMany
            private List<String> emails;

            public Long getId() {
                return id;
            }

            public void setId(Long id) {
                this.id = id;
            }

            public String getNome() {
                return nome;
            }

            public void setNome(String nome) {
                this.nome = nome;
            }

            public Endereco getEndereco() {
                return endereco;
            }

            public void setEndereco(Endereco endereco) {
                this.endereco = endereco;
            }

            public List<String> getEmails() {
                return emails;
            }

            public void setEmails(List<String> emails) {
                this.emails = emails;
            }

        }

        List<String> fields = Arrays.asList("id", "nome", "endereco");
        Map<String, Field[]> result = FieldExtractor.extractFields(Usuario.class, fields);
        printMap(result);  // Saída esperada: [id, nome, endereco.rua, endereco.cidade]

        Map<String, Field[]> emptyFieldsResult = FieldExtractor.extractFields(Usuario.class, new ArrayList<>());
        printMap(emptyFieldsResult);
    }

    public static void printMap(Map<String, Field[]> fieldsResult) {
        for (Map.Entry<String, Field[]> entry : fieldsResult.entrySet()) {
            String key = entry.getKey();
            Field[] fields = entry.getValue();

            // Imprime cada campo no array de Fields
            System.out.print("Key: " + key + " : ");
            for (int i = 0; i < fields.length; i++) {
                Field field = fields[i];
                System.out.print(" [" + i + "]" + field.getDeclaringClass().getSimpleName() + "." + field.getName());
            }
            System.out.println();
        }
    }

}
