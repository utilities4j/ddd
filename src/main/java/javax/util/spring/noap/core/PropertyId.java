package javax.util.spring.noap.core;

import jakarta.persistence.*;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.util.ddd.infra.util.Throw;

/**
 * Classe utilitária para trabalhar com propriedades identificadoras de entidades JPA.
 *
 * @author Marcius Brandão
 */
public class PropertyId {

    // Cache dos campos identificadores por classe.
    private static final Map<Class<?>, Field> idFieldCache = new ConcurrentHashMap<>();

    /**
     * Classe interna que representa uma propriedade identificadora.
     */
    public static record IdProperty(String name, Object value, boolean generatedValue) {

    }

    /**
     * Obtém a propriedade identificadora em uma classe.
     *
     * @param <T> tipo da classe
     * @param entity A entidade da qual se deseja obter a propriedade identificadora.
     * @return Uma instância de IdProperty representando a propriedade identificadora da entidade.
     */
    public static <T> IdProperty getIdProperty(final T entity) {
        Throw.ifIsNull(entity, "Entity instance cannot be null");

        // Obtém o campo identificador da entidade e verifica se é nulo.
        Field idField = getIdField(entity.getClass());
        Throw.ifIsNull(idField, entity.getClass().getCanonicalName() + " has no property annotated with @Id");

        try {
            // Obtém o valor do campo identificador e verifica se é um valor gerado automaticamente.
            Object idValue = idField.get(entity);
            boolean generatedValue = idField.isAnnotationPresent(GeneratedValue.class);
            return new IdProperty(idField.getName(), idValue, generatedValue);
        } catch (IllegalAccessException ex) {
            throw new RuntimeException("The @Id property of the " + entity.getClass().getCanonicalName() + " entity cannot be accessed.", ex);
        }
    }

    /**
     * Obtém o nome do campo identificador para uma classe específica.
     *
     * @param clazz A classe da qual se deseja obter o nome do campo identificador.
     * @return O nome do campo identificador.
     */
    public static String getIdFieldName(Class<?> clazz) {
        Field idField = getIdField(clazz);
        Throw.ifIsNull(idField, "No field annotated with @Id found in class hierarchy of " + clazz.getName());
        return idField.getName();
    }

    /**
     * Obtém o valor do campo identificador para uma entidade específica.
     *
     * @param <T> Tipo da classe
     * @param entity A entidade da qual se deseja obter o valor do campo identificador.
     * @return O valor do campo identificador.
     * @throws IllegalAccessException Se o acesso ao campo identificador não for possível.
     */
    public static <T> Object getIdFieldValue(T entity) throws IllegalAccessException {
        Field idField = getIdField(entity.getClass());
        Throw.ifIsNull(idField, "No field annotated with @Id found in class hierarchy of " + entity.getClass().getName());
        return idField.get(entity);
    }

    // Obtém o campo identificador de uma classe.
    public static Field getIdField(Class<?> clazz) {
        return idFieldCache.computeIfAbsent(clazz, cls -> {
            Class<?> currentClass = cls;
            while (currentClass != null) {
                // Percorre os campos da classe atual e suas superclasses em busca do campo identificador.
                for (Field field : currentClass.getDeclaredFields()) {
                    if (field.isAnnotationPresent(Id.class) || field.isAnnotationPresent(EmbeddedId.class)) {
                        field.setAccessible(true);
                        return field;
                    }
                }
                // Move para a superclasse para continuar a busca.
                currentClass = currentClass.getSuperclass();
            }
            return null;
        });
    }

    public static void setPropertyID(Object entity, Object id) throws Exception {
        Field fieldId = getIdField(entity.getClass());
        fieldId.setAccessible(true);
        fieldId.set(entity, id);
    }
}
