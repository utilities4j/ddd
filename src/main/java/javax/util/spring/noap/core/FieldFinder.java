package javax.util.spring.noap.core;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;

public class FieldFinder {

    public static Field findField(Class<?> clazz, String name) {
        // Divida o nome do campo por pontos para lidar com campos aninhados
        String[] fieldNames = name.split("\\.");

        // Chamamos a função auxiliar recursiva para buscar o campo
        return findFieldRecursively(clazz, fieldNames, 0);
    }

    private static Field findFieldRecursively(Class<?> clazz, String[] fieldNames, int index) {
        if (clazz == null || index >= fieldNames.length) {
            return null;
        }

        Field field;
        try {
            field = clazz.getDeclaredField(fieldNames[index]);
        } catch (NoSuchFieldException e) {
            return null;
        }

        // Verificar se existe um getter para o campo
        String getterName = "get" + capitalize(fieldNames[index]);
        try {
            clazz.getMethod(getterName);
        } catch (NoSuchMethodException e) {
            return null;
        }

        // Se for o último nome no array, retornamos o campo encontrado
        if (index == fieldNames.length - 1) {
            return field;
        }

        // Verificar se o campo é uma coleção ou um array e obter o tipo do elemento
        Class<?> fieldType = field.getType();
        if (Collection.class.isAssignableFrom(fieldType)) {
            // Obter o tipo de elemento da coleção
            fieldType = getGenericType(field);
        } else if (fieldType.isArray()) {
            // Obter o tipo do componente do array
            fieldType = fieldType.getComponentType();
        }

        // Prosseguir recursivamente para a próxima parte do campo aninhado
        return findFieldRecursively(fieldType, fieldNames, index + 1);
    }

    private static String capitalize(String str) {
        if (str == null || str.isEmpty()) {
            return str;
        }
        return Character.toUpperCase(str.charAt(0)) + str.substring(1);
    }

    private static Class<?> getGenericType(Field field) {
        try {
            return (Class<?>) ((java.lang.reflect.ParameterizedType) field.getGenericType())
                    .getActualTypeArguments()[0];
        } catch (Exception e) {
            return Object.class; // Fallback para Object se não for possível determinar o tipo genérico
        }
    }

    public static void main(String[] args) {

        class Cidade {

            private String nome;

            public String getNome() {
                return nome;
            }

        }
        class Endereco {

            private Cidade cidade;

            public Cidade getCidade() {
                return cidade;
            }

        }
        class Pessoa {

            private String nome;
            private Endereco endereco;

            public String getNome() {
                return nome;
            }

            public Endereco getEndereco() {
                return endereco;
            }

        }

        Field cidadeField = findField(Pessoa.class, "endereco.cidade.nome");
        System.out.println(cidadeField);
        class Item {

            private String id;

            public String getId() {
                return id;
            }
        }
        
        class Pedido {

            private List<Item> items;

            public List<Item> getItems() {
                return items;
            }
        }
        
           // Exemplo de uso
        Field field = findField(Pedido.class, "items.id");
        if (field != null) {
            System.out.println("Campo encontrado: " + field.getName());
        } else {
            System.out.println("Campo não encontrado");
        }

    }
//        // Exemplo de uso
//        Field field = findField(Pessoa.class, "endereco.cidade.nome");
//        if (field != null) {
//            System.out.println("Campo encontrado: " + field.getName());
//        } else {
//            System.out.println("Campo não encontrado");
//        }
//    }
}
