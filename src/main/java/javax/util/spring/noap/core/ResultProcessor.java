package javax.util.spring.noap.core;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static javax.util.spring.noap.core.Reflections.isAssociation;
import static javax.util.spring.noap.core.Reflections.isCollection;

public class ResultProcessor {

    public static <T> List<T> processResultListToEntity(List<Object[]> resultList, List<String> fields, Class<T> entityClass) {
        if (resultList == null || resultList.isEmpty()) {
            return new ArrayList<>();
        } else if (resultList.get(0) instanceof Object[]) {
            return resultList.stream()
                    .map(row -> processSingleResultToEntity((Object[]) row, fields, entityClass))
                    .collect(Collectors.toList());
        }

        throw new IllegalArgumentException("Invalid result type. Must be List<Object[]> or Object[].");
    }

    private static <T> T processSingleResultToEntity(Object[] row, List<String> fields, Class<T> entityClass) {
        try {
            T entity = entityClass.getDeclaredConstructor().newInstance();
            for (int i = 0; i < fields.size(); i++) {
                String fieldName = fields.get(i);
                Object value = row[i];

                Field field = entityClass.getDeclaredField(fieldName);
                field.setAccessible(true);
                field.set(entity, value);
            }
            return entity;
        } catch (Exception e) {
            throw new RuntimeException("Error mapping result to entity: " + e.getMessage(), e);
        }
    }

    //cria mapas aninhados para associações e agrupa campos aninhados em listas de mapas.
    public static List<Map<String, Object>> consolidateResults(Class entityClass, List<Object[]> resultList, Map<String, Field[]> fieldsMap) {
        if (resultList == null || fieldsMap == null) {
            throw new IllegalArgumentException("resultList and fields cannot be null");
        }

        final List<String> fieldsNames = new ArrayList<>(fieldsMap.keySet());//temporario
        List<Map.Entry<String, Field[]>> listFieldsMap = new ArrayList<>(fieldsMap.entrySet());

        Map<Object, Map<String, Object>> consolidatedResults = new LinkedHashMap<>();
        for (Object[] result : resultList) {
            if (result.length != fieldsMap.size()) {
                throw new IllegalArgumentException("Each result must have the same number of elements as there are fields");
            }

            Object mainEntityId = result[0];
            Map<String, Object> resultMap = consolidatedResults.computeIfAbsent(mainEntityId, k -> new LinkedHashMap<>());
            for (int i = 0; i < listFieldsMap.size(); i++) {
                Map.Entry<String, Field[]> entry = listFieldsMap.get(i);
                String fieldname = entry.getKey();
                Field[] aFields = entry.getValue();
                Object value = result[i];

                if (fieldname.contains(".")) {
                    handleNestedField(resultMap, fieldsNames, result, fieldname, value, aFields);
                } else {
                    resultMap.put(fieldname, value);
                }
            }
        }
        return new ArrayList<>(consolidatedResults.values());
    }

    private static void handleNestedField(Map<String, Object> resultMap, List<String> fieldsNames, Object[] result, String fieldName, Object value, Field[] aFields) {
        String[] nestedFields = fieldName.split("\\.");
        String nestedFieldName = nestedFields[0];

        Map<String, Object> mapdeepest = resultMap;
        for (int i = 0; i < aFields.length - 1; i++) {
            Field nestedField = aFields[i];
            if (isCollection(nestedField)) {
                List<Map<String, Object>> nestedList = (List<Map<String, Object>>) resultMap.computeIfAbsent(nestedField.getName(), k -> new ArrayList<>());
                
                //ja tem o id da collection?
                Map<Object, Map<String, Object>> nestedEntityMap = nestedList.stream().collect(Collectors.toMap(map -> map.get("id"), map -> map));
                final int idIndex = getFieldIndex(fieldsNames, nestedFieldName + ".id");
                Object nestedEntityId = result[idIndex];
                Map<String, Object> nestedMap = nestedEntityMap.get(nestedEntityId);
                if (nestedMap == null) {
                    nestedMap = new LinkedHashMap<>();
                    nestedList.add(nestedMap);
                    nestedMap.put("id", nestedEntityId);
                }
                mapdeepest = nestedMap;

            } else if (isAssociation(nestedField)) {
                mapdeepest = (Map<String, Object>) mapdeepest.computeIfAbsent(nestedField.getName(), k -> new LinkedHashMap<>());
            }
        }

        mapdeepest.put(aFields[aFields.length - 1].getName(), value);
    }

    //ajuda a encontrar o índice de um campo aninhado, necessário para agrupar corretamente os resultados.
    private static int getFieldIndex(List<String> fields, String fieldName) {
        for (int i = 0; i < fields.size(); i++) {
            if (fields.get(i).equals(fieldName)) {
                return i;
            }
        }
        return -1;
    }

}
