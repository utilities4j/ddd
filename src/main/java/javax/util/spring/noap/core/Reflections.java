package javax.util.spring.noap.core;

import jakarta.persistence.Embedded;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Version;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import javax.util.ddd.infra.util.Throw;

public class Reflections {

    private static final Map<Class<?>, Field[]> declaredFieldsCache = new ConcurrentHashMap<>(256);
    private static final Field[] EMPTY_FIELD_ARRAY = new Field[0];

    public static Optional<Method> findMethodWithParams(Class<?> clazz, String methodName, Map<String, Object> args) {
        for (Method method : clazz.getMethods()) {
            if (method.getName().equals(methodName) && method.getParameterCount() == args.size()) {
                return Optional.of(method);
            }
        }
        return Optional.empty();
    }

    public static boolean isCollection(Field field) {
        return Collection.class.isAssignableFrom(field.getType());
    }

    public static boolean isBidirectional(Field field) {
        return field.isAnnotationPresent(ManyToOne.class) 
                || field.isAnnotationPresent(OneToOne.class) 
                || field.isAnnotationPresent(OneToMany.class) 
                || field.isAnnotationPresent(ManyToMany.class);
    }
    
    public static boolean isId(Field field) {
        return field.isAnnotationPresent(Id.class);
    }

    public static List<String> getDeclaredFieldsWithGetter(Class<?> entityClass) throws SecurityException {
        List<String> fields;
        fields
                = //.filter(field -> !isBidirectional(field)) @TODO
                Arrays.stream(entityClass.getDeclaredFields()).filter(field -> hasGetter(entityClass, field)).filter(field -> !isCollection(field)).filter(field -> !isVersionField(field)).map(Field::getName).collect(Collectors.toList());
        return fields;
    }

    public static boolean hasGetter(Class<?> clazz, Field field) {
        String fieldName = field.getName();
        String getterName = "get" + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
        try {
            Method getter = clazz.getMethod(getterName);
            // Verifique se o método `getter` existe e retorna o tipo correto
            return getter.getReturnType().equals(field.getType());
        } catch (NoSuchMethodException e) {
            // O método `getter` não existe
            return false;
        }
    }

    public static boolean isVersionField(Field field) {
        return field.isAnnotationPresent(Version.class);
    }

    /**
     * Attempt to find a {@link Field field} on the supplied {@link Class} with the supplied
     * {@code name}. Searches all superclasses up to {@link Object}.
     *
     * @param clazz the class to introspect
     * @param name the name of the field
     * @return the corresponding Field object, or {@code null} if not found
     */
    public static Field findField(Class<?> clazz, String name) {
        return findField(clazz, name, null);
    }

    /**
     * Attempt to find a {@link Field field} on the supplied {@link Class} with the supplied
     * {@code name} and/or {@link Class type}. Searches all superclasses up to {@link Object}.
     *
     * @param clazz the class to introspect
     * @param name the name of the field (may be {@code null} if type is specified)
     * @param type the type of the field (may be {@code null} if name is specified)
     * @return the corresponding Field object, or {@code null} if not found
     */
    public static Field findField(Class<?> clazz, String name, Class<?> type) {
        Throw.ifIsNull(clazz, "Class must not be null");
        Throw.ifIsTrue(name == null && type == null, "Either name or type of the field must be specified");

        Class<?> searchType = clazz;
        while (Object.class != searchType && searchType != null) {
            Field[] fields = getDeclaredFields(searchType);
            for (Field field : fields) {
                if ((name == null || name.equals(field.getName()))
                        && (type == null || type.equals(field.getType()))) {
                    return field;
                }
            }
            searchType = searchType.getSuperclass();
        }
        return null;
    }

    /**
     * This variant retrieves {@link Class#getDeclaredFields()} from a local cache in order to avoid
     * defensive array copying.
     *
     * @param clazz the class to introspect
     * @return the cached array of fields
     * @throws IllegalStateException if introspection fails
     * @see Class#getDeclaredFields()
     */
    private static Field[] getDeclaredFields(Class<?> clazz) {
        Throw.ifIsNull(clazz, "Class must not be null");
        Field[] result = declaredFieldsCache.get(clazz);
        if (result == null) {
            try {
                result = clazz.getDeclaredFields();
                declaredFieldsCache.put(clazz, (result.length == 0 ? EMPTY_FIELD_ARRAY : result));
            } catch (Throwable ex) {
                throw new IllegalStateException("Failed to introspect Class [" + clazz.getName()
                        + "] from ClassLoader [" + clazz.getClassLoader() + "]", ex);
            }
        }
        return result;
    }

    /**
     *
     * Garante que a propriedade só será alterada se o método setter existir, respeitando o
     * encapsulamento e podendo incluir lógica adicional na definição da propriedade.
     *
     * @param object
     * @param field
     * @param value
     * @throws Exception
     */
    public static void setProperty(Object object, Field field, Object value) throws Exception {
        String setterName = "set" + capitalize(field.getName());
        Method setterMethod = object.getClass().getMethod(setterName, field.getType());
        setterMethod.invoke(object, convertToFieldType(value, field.getType()));
    }

    private static String capitalize(String str) {
        if (str == null || str.isEmpty()) {
            return str;
        }
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    public static Object convertToFieldType(Object value, Class<?> fieldType) {
        if (value == null) {
            return null;
        }

        if (fieldType.isInstance(value)) {
            return value;
        }

        if (fieldType.equals(String.class)) {
            return value.toString();
        }

        if (fieldType.equals(int.class) || fieldType.equals(Integer.class)) {
            return Integer.valueOf(value.toString());
        }

        if (fieldType.equals(long.class) || fieldType.equals(Long.class)) {
            return Long.valueOf(value.toString());
        }

        if (fieldType.equals(double.class) || fieldType.equals(Double.class)) {
            return Double.valueOf(value.toString());
        }

        if (fieldType.equals(boolean.class) || fieldType.equals(Boolean.class)) {
            return Boolean.valueOf(value.toString());
        }

        if (fieldType.equals(String.class)) {
            return value.toString();
        }

        if (fieldType.equals(Date.class)) {
            String dateFormat = "yyyy-MM-dd"; // Ajuste o formato conforme necessário
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            try {
                return sdf.parse(value.toString());
            } catch (ParseException e) {
                throw new RuntimeException("Failed to parse date: " + value, e);
            }
        }

        // Adicione mais tipos conforme necessário
        // Se nenhum dos tipos acima corresponder, retorne o valor original
        return value;
    }

    public static boolean isAssociation(Field field) {
        return field.isAnnotationPresent(OneToOne.class) 
                || field.isAnnotationPresent(OneToMany.class)
                || field.isAnnotationPresent(ManyToOne.class) 
                || field.isAnnotationPresent(ManyToMany.class) 
                || field.isAnnotationPresent(Embedded.class);
    }
}
