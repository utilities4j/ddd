package javax.util.spring.noap.core;

import jakarta.persistence.Transient;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class FieldFinderNested {

    public static Field[] findAllFields(Class<?> clazz, String name) {
        // Divida o nome do campo por pontos para lidar com campos aninhados
        String[] fieldNames = name.split("\\.");

        List<Field> fields = new ArrayList<>();
        findAllFieldsRecursively(clazz, fieldNames, 0, fields);

        return fields.toArray(Field[]::new);
    }

    private static void findAllFieldsRecursively(Class<?> clazz, String[] fieldNames, int index, List<Field> fields) {
        if (clazz == null || index >= fieldNames.length) {
            return;
        }

        Field field;
        try {
            field = clazz.getDeclaredField(fieldNames[index]);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(new NoSuchFieldException(concatenateFieldNames(fieldNames, index)));
        }

        // Verificar se existe um getter para o campo
        String getterName = "get" + capitalize(fieldNames[index]);
        try {
            clazz.getMethod(getterName);
        } catch (NoSuchMethodException e) {
            return;
        }

        if (field.isAnnotationPresent(Transient.class)) {
            return;
        }
        
        fields.add(field);
        // Verificar se o campo é uma coleção ou um array e obter o tipo do elemento
        Class<?> fieldType = field.getType();
        if (Collection.class.isAssignableFrom(fieldType)) {
            // Obter o tipo de elemento da coleção
            fieldType = getGenericType(field);
        } else if (fieldType.isArray()) {
            // Obter o tipo do componente do array
            fieldType = fieldType.getComponentType();
        }

        // Prosseguir recursivamente para a próxima parte do campo aninhado
        findAllFieldsRecursively(fieldType, fieldNames, index + 1, fields);
    }

    private static String capitalize(String str) {
        if (str == null || str.isEmpty()) {
            return str;
        }
        return Character.toUpperCase(str.charAt(0)) + str.substring(1);
    }

    private static Class<?> getGenericType(Field field) {
        try {
            return (Class<?>) ((java.lang.reflect.ParameterizedType) field.getGenericType())
                    .getActualTypeArguments()[0];
        } catch (Exception e) {
            return Object.class; // Fallback para Object se não for possível determinar o tipo genérico
        }
    }

    private static String concatenateFieldNames(String[] fieldNames, int index) {
        if (fieldNames == null || index < 0 || index >= fieldNames.length) {
            throw new IllegalArgumentException("Invalid index or fieldNames array.");
        }

        StringBuilder result = new StringBuilder();
        for (int i = 0; i <= index; i++) {
            if (i > 0) {
                result.append('.');
            }
            result.append(fieldNames[i]);
        }

        return result.toString();
    }

    public static void main(String[] args) {

        class Cidade {

            private String nome;

            public String getNome() {
                return nome;
            }

        }
        class Endereco {

            private Cidade cidade;

            public Cidade getCidade() {
                return cidade;
            }

        }
        class Pessoa {

            private String nome;
            private Endereco endereco;

            public String getNome() {
                return nome;
            }

            public Endereco getEndereco() {
                return endereco;
            }

        }

        Field[] cidadeField = findAllFields(Pessoa.class, "endereco.cidade.nome");
        System.out.println(Arrays.toString(cidadeField));

        Field[] cidadeField2 = findAllFields(Pessoa.class, "nome");
        System.out.println(Arrays.toString(cidadeField2));

        class Item {

            private String id;

            public String getId() {
                return id;
            }
        }

        class Pedido {

            private List<Item> items;

            public List<Item> getItems() {
                return items;
            }
        }

        // Exemplo de uso
        Field[] field = findAllFields(Pedido.class, "items.id");
        if (field != null) {
            System.out.println("Campo encontrado: " + Arrays.toString(field));
        } else {
            System.out.println("Campo não encontrado");
        }

    }
}
