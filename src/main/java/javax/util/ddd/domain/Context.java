package javax.util.ddd.domain;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class Context {

    private static Context instance;
    protected final Map<String, Object> contextData = new ConcurrentHashMap<>();
    private static final Logger LOG = Logger.getLogger(Context.class.getName());

    protected Context() {
        if (instance == null) {
            instance = this;
            initialize();
        } else {
            LOG.warning("context already initialized");
        }
    }

    public abstract void initialize(); // Método abstrato para inicialização

    public static Context getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Instance not created yet!");
        }
        return instance;
    }

    //<editor-fold defaultstate="collapsed" desc="get/set/clear values">
    public void setValue(String key, Object value) {
        contextData.put(key, value);
    }
    
    public Object getValue(String key) {
        return contextData.get(key);
    }
    
    public void clear() {
        contextData.clear();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="CurrentUser">
    private static final String CURRENT_USER_KEY = "currentUser";

    public void setCurrentUser(Object user) {
        setValue(CURRENT_USER_KEY, user);
    }

    public Object getCurrentUser() {
        return getValue(CURRENT_USER_KEY);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Repository">
    @Autowired
    private IRepository repository;

    public IRepository getRepository() {
        return repository;
    }

    public void setRepository(IRepository repository) {
        this.repository = repository;
    }
    //</editor-fold>

}
