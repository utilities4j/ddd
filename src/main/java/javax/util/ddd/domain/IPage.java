package javax.util.ddd.domain;

import java.util.List;

/**
 *
 * @author Marcius Brandão
 * @param <E>
 */
public interface IPage<E> {

    /*
     * Inteface Slice do Spring
     *
     */
    List<E> getContent();

    int getNumber();

    int getNumberOfElements();

    int getPageNumber();

    /*
     * Interface Pageable
     */
    int getPageSize();

    int getSize();

    long getTotalElements();

    /*
     * Interface Page do Spring
     *    public int getTotalPages();
     *    public long getTotalElements();
     */
    int getTotalPages();

    boolean isFirst();

    boolean isHasContent();

    boolean isHasNext();

    boolean isHasPrevious();

    boolean isLast();
    
}
