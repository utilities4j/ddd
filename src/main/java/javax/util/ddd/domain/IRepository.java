package javax.util.ddd.domain;

import java.util.List;
import java.util.Optional;

/**
 * Padrão repository conforme DDD - Out of Box Entities<br>
 * - JPA (Hibernate,eclipseLink,...)<br>
 * - In memmory (javadb) <br>
 * - Multi BD <br>
 * - Paginação, Filter e Order by <br>
 * - Projeção <br>
 * - Transaction Nested <br>
 * - REST <br>
 *
 * @author Marcius
 */
public interface IRepository {

    void add(Object entity);
    
    int add(String jpql, Object... params);

    void addAll(Object... entities);

    void set(Object entity);

    void setAll(Object... entities);

    int setAll(String jpql, Object... params);
    
    void remove(Object entity);

    void remove(Class<?> entityClass, Object id);

    void removeAll(Object... entities);
    
    int removeAll(String jpql, Object... params);

    boolean contains(Object entity);
    
    boolean contains(Class<?> entityClass, Object id);
            
    <E> Optional<E> get(Class<E> entityClass, Object id);

    <E> Optional<E> get(String jpql, Object... params);

    <E> List<E> getAll(Class<E> entityClass);

    <E> List<E> getAll(String jpql, Object... params);

    <E> IPage<E> subList(String jpql, int pageSize, int pageIndex, Object... params);
    
    <E> IPage<E> subList(Class<E> entityClass, int pageSize, int pageIndex, Object... params);

    long size(String jpql, Object... params);

    void clear();

    void persistAll();
    
    Optional<List<String>> validate(Object entity);
}
