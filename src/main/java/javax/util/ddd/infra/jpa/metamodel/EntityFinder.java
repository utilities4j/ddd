package javax.util.ddd.infra.jpa.metamodel;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.metamodel.EntityType;
import jakarta.persistence.metamodel.Metamodel;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class EntityFinder {

    private static final Logger LOG = Logger.getLogger(EntityFinder.class.getName());
    private final Map<String, Class<?>> cache = new ConcurrentHashMap<>();
    private volatile boolean isCacheFullyLoaded = false;

    @PersistenceContext
    private final EntityManager entityManager;

    public EntityFinder(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Optional<Class<?>> findEntityClass(String simpleName) {
        // Se não estiver no cache e o cache não estiver totalmente carregado, carrega todas as entidades
        if (!isCacheFullyLoaded) { // Primeira verificação sem bloqueio
            synchronized (this) {
                if (!isCacheFullyLoaded) { // Segunda verificação com bloqueio double-checked locking
                    Metamodel metamodel = entityManager.getMetamodel();
                    Set<EntityType<?>> entities = metamodel.getEntities();
                    for (EntityType<?> entityType : entities) {
                        Class<?> javaType = entityType.getJavaType();
                        cache.putIfAbsent(javaType.getSimpleName(), javaType);
                        LOG.log(Level.INFO, "Mapeando {0}->{1}", new Object[]{javaType.getSimpleName(), javaType.getCanonicalName()});
                    }
                    isCacheFullyLoaded = true;
                }
            }
        }

        // Retorna o resultado usando Optional para encapsular a possibilidade de null
        return Optional.ofNullable(cache.get(simpleName));
    }
    
     public Set<Class<?>> getClasses() {
        return cache.values().stream().collect(Collectors.toSet());
    }
}
