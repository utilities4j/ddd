package javax.util.ddd.infra.jpa;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JPQL {

    final private String jpql;

    public JPQL(String jpql) {
        if (jpql == null || jpql.trim().isEmpty()) {
            throw new IllegalArgumentException("Provided JPQL query is empty or null.");
        }
        String lowerCaseJpql = jpql.trim().toLowerCase();
        if (!(lowerCaseJpql.startsWith("select") || lowerCaseJpql.startsWith("from"))) {
            throw new IllegalArgumentException("Provided JPQL query does not start with 'SELECT' or 'FROM', not valid for a SELECT operation.");
        }
        this.jpql = jpql;
    }

    public List<String> extrairPropriedadesEAliasDoSelect() {
        int startIndex = jpql.toUpperCase().indexOf("SELECT") + "SELECT".length();
        int endIndex = jpql.toUpperCase().indexOf("FROM");
        if (startIndex == -1 || endIndex == -1 || startIndex >= endIndex) {
            return new ArrayList<>();  // Retorna lista vazia se índices não forem válidos
        }

        String selectClause = jpql.substring(startIndex, endIndex).trim();

        // Regex melhorada para capturar propriedades e aliases mais complexos
        Pattern pattern = Pattern.compile("\\b([\\w\\.]+)(?:\\s+as\\s+(\\w+))?", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(selectClause);

        List<String> propriedadesEAlias = new ArrayList<>();
        while (matcher.find()) {
            if (matcher.group(2) != null) {
                propriedadesEAlias.add(matcher.group(2));  // Adiciona apenas o alias
            } else {
                propriedadesEAlias.add(matcher.group(1));  // Adiciona a propriedade se não houver alias
            }
        }
        return propriedadesEAlias;
    }

    public Map<String, String> extrairEntidadesEAliasDoFrom() {
        Map<String, String> entities = new LinkedHashMap<>();
        Pattern pattern = Pattern.compile("FROM\\s+(.*?)(?:\\s+WHERE|\\s+GROUP BY|\\s+ORDER BY|\\s+HAVING|\\s*$)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher matcher = pattern.matcher(jpql);
        if (matcher.find()) {
            String fromClause = matcher.group(1).trim();
            String[] parts = fromClause.split(",");
            for (String part : parts) {
                part = part.trim();
                String[] split = part.split("\\s+");
                String alias = split.length > 1 ? split[1] : split[0];
                entities.put(split[0], alias);
            }
        }
        return entities;
    }

    public Map<String, String> extrairEntidadesEAliasDosJoins() {
        // Atualização da Regex para incluir "FETCH" nos padrões de JOIN
        Pattern joinPattern = Pattern.compile(
                "\\b(JOIN|LEFT JOIN|LEFT OUTER JOIN|RIGHT JOIN|RIGHT OUTER JOIN|INNER JOIN|OUTER JOIN|FETCH JOIN|INNER JOIN FETCH|LEFT JOIN FETCH|RIGHT JOIN FETCH)\\s+(\\w+\\.\\w+)\\s+(?:AS\\s+)?(\\w+)?",
                Pattern.CASE_INSENSITIVE);

        Matcher joinMatcher = joinPattern.matcher(jpql);
        Map<String, String> entidadesEAlias = new LinkedHashMap<>();
        while (joinMatcher.find()) {
            String path = joinMatcher.group(2);
            String alias = joinMatcher.group(3);

            // Verificar e limpar o alias se ele contiver palavras-chave SQL
            if (alias != null && alias.matches("(?i)(WHERE|ON|ORDER|GROUP|HAVING|FETCH|JOIN)")) {
                alias = null;
            }

            // Se o alias não foi fornecido ou é inválido, usa o último segmento do caminho como alias
            if (alias == null || alias.isBlank()) {
                alias = path.substring(path.lastIndexOf('.') + 1);  // Assume last part of path as alias
            }

            entidadesEAlias.put(path, alias);
        }
        return entidadesEAlias;
    }

    public List<String> extrairNomesParametros() {
        int whereIndex = jpql.toUpperCase().indexOf("WHERE");
        if (whereIndex == -1) {
            return new ArrayList<>();  // Retorna lista vazia se WHERE não estiver presente
        }

        String whereClause = jpql.substring(whereIndex);

        // Pattern para capturar tanto parâmetros nomeados como posicionais
        Pattern pattern = Pattern.compile(":(\\w+)|\\?(\\d+)");
        Matcher matcher = pattern.matcher(whereClause);

        List<String> parametros = new ArrayList<>();
        while (matcher.find()) {
            // Adiciona o nome do parâmetro ou o número do parâmetro posicional
            if (matcher.group(1) != null) {
                parametros.add(":" + matcher.group(1));
            } else if (matcher.group(2) != null) {
                parametros.add("?" + matcher.group(2));
            }
        }
        return parametros;
    }

    public String modificarConsultaJPQL(List<String> condicoesWhere, List<String> camposOrderBy) {
        String consultaAtualizada = adicionarCondicionalWhere(condicoesWhere);
        //consultaAtualizada = adicionarCamposOrderBy(consultaAtualizada, camposOrderBy);
        return consultaAtualizada;
    }

    public String adicionarCondicionalWhere(List<String> novasCondicoes) {
        StringBuilder query = new StringBuilder(jpql);
        if (!novasCondicoes.isEmpty()) {
            boolean hasWhere = jpql.toUpperCase().contains(" WHERE ");
            if (hasWhere) {
                query.append(" AND ");
            } else {
                query.append(" WHERE ");
            }
            query.append(String.join(" AND ", novasCondicoes));
        }
        return query.toString();
    }

    public String adicionarCamposOrderBy(List<String> campos) {
        StringBuilder query = new StringBuilder(jpql);
        if (!campos.isEmpty()) {
            boolean hasOrderBy = jpql.toUpperCase().contains(" ORDER BY ");
            if (hasOrderBy) {
                query.append(", ");
            } else {
                query.append(" ORDER BY ");
            }
            query.append(String.join(", ", campos));
        }
        return query.toString();
    }

    public static String transformarParaConsultaDeContagem(String jpqlOriginal) {
        // Encontrar o início da cláusula FROM
        int fromIndex = jpqlOriginal.toUpperCase().indexOf("FROM");
        if (fromIndex == -1) {
            throw new IllegalArgumentException("Consulta JPQL inválida: sem FROM");
        }

        // Remover cláusulas ORDER BY se presentes
        int orderByIndex = jpqlOriginal.toUpperCase().indexOf("ORDER BY");
        String jpqlSemOrder = (orderByIndex == -1) ? jpqlOriginal : jpqlOriginal.substring(0, orderByIndex);

        // Substituir a seleção pela função COUNT
        String jpqlContagem = "SELECT COUNT(*) "
                + jpqlSemOrder.substring(fromIndex).
                        replaceAll("fetch all properties", " ").
                        replaceAll(" fetch ", " ");

        return jpqlContagem;
    }

    
}
