package javax.util.ddd.infra.jpa.util;

import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.Persistence;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author Marcius
 * @version 1.0
 *
 */
public class PersistenceUnitFinder {

    private static final Logger LOG = Logger.getLogger(PersistenceUnitFinder.class.getName());
    private static List<PersistenceUnit> persistenceUnits;

    static {
        try {
            persistenceUnits = parsePersistenceUnit();
        } catch (Exception ex) {
            Logger.getLogger(PersistenceUnitFinder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static EntityManagerFactory getEntityManagerFactory() {
        return getEntityManagerFactory(null);
    }

    public static EntityManagerFactory getEntityManagerFactory(String persistenceUnitName) {
        if (persistenceUnitName == null) {
            return persistenceUnits.get(0).getEntityManagerFactory();
        } else {
            for (PersistenceUnit pu : persistenceUnits) {
                if (pu.getPersistencyUnit().equals(persistenceUnitName)) {
                    return pu.getEntityManagerFactory();
                }
            }
        }
        throw new IllegalArgumentException("PersisceUnit " + persistenceUnitName + " não encontrada");
    }

    private static List<PersistenceUnit> parsePersistenceUnit() throws Exception {
        List<PersistenceUnit> pu = new LinkedList<>();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(ClassLoader.getSystemResourceAsStream("META-INF/persistence.xml"));

        NodeList nodeList = doc.getElementsByTagName("persistence-unit");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element element = (Element) nodeList.item(i);
            String puName = element.getAttribute("name");
            pu.add(new PersistenceUnit(puName, Persistence.createEntityManagerFactory(puName)));
        }

        return pu;
    }
    
//    public static void closeEntityManagerFactory() {
//        if (entityManagerFactory != null && entityManagerFactory.isOpen()) {
//            entityManagerFactory.close();
//        }
//    }
}
