package javax.util.ddd.infra.jpa.util;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.NamedQueries;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
//import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.NamedQueries;

/**
 *
 * @author Marcius
 */
class PersistenceUnit {

    public static final Map<Class<?>, PersistenceUnit> mapClassPU = new HashMap<>();
    public static final Map<String, String> mapNamedQueryOQL = new HashMap<>();
    public static final Map<String, Class<?>> mapNamedQueryClass = new HashMap<>();

    private final String persistencyUnit;
    private final EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    public PersistenceUnit(String persistencyUnit, EntityManagerFactory entityManagerFactory) {
        this.persistencyUnit = persistencyUnit;
        this.entityManagerFactory = entityManagerFactory;
        loadMappedClasses();
    }

    private void loadMappedClasses() {
        this.getEntityManagerFactory().getMetamodel().getManagedTypes().forEach((managedType) -> {
            Class<?> clazz = managedType.getJavaType();
            mapClassPU.put(managedType.getJavaType(), this);

            NamedQueries jpaNamedQueries = (NamedQueries) clazz.getAnnotation(NamedQueries.class);
            if (jpaNamedQueries != null) {
                //for (javax.persistence.NamedQuery namedQuery : jpaNamedQueries.value()) {
                for (jakarta.persistence.NamedQuery namedQuery : jpaNamedQueries.value()) {
                    mapNamedQueryOQL.put(namedQuery.name(), namedQuery.query());
                    mapNamedQueryClass.put(namedQuery.name(), clazz);
                }
            }

            if (clazz.getCanonicalName().contains(".")) {
                String[] split = clazz.getCanonicalName().split("\\.");
                mapNamedQueryClass.put(split[split.length - 1], clazz);
            }
        });
    }

    public String getPersistencyUnit() {
        return persistencyUnit;
    }

    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.persistencyUnit);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PersistenceUnit other = (PersistenceUnit) obj;
        return Objects.equals(this.persistencyUnit, other.persistencyUnit);
    }

}
