package javax.util.ddd.infra.jpa.util;

import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * <pre>
 * 1. Inicialização
 * A classe EntityManagerFactoryRegistry é um Singleton,
 * então você a acessa através de seu método getInstance().
 * A inicialização ocorre automaticamente a primeira vez que este método é chamado,
 * garantindo que todas as EntityManagerFactory sejam criadas conforme definido no método initializeFactories().
 *
 * Exemplo:  EntityManagerFactoryRegistry registry = EntityManagerFactoryRegistry.getInstance();
 *
 * 2. Obter uma EntityManagerFactory
 * Para executar operações no banco de dados, você precisa de uma EntityManager.
 * Primeiro, obtenha a EntityManagerFactory para a unidade de persistência desejada usando getFactory(String unitName).
 * exemplo:
 *   EntityManagerFactory emf = registry.getFactory("MyPersistenceUnit");
 *   EntityManager em = emf.createEntityManager();
 *
 * 3. Usar EntityManager para Operações de Banco de Dados
 * Com o EntityManager, você pode começar a realizar operações de banco de dados,
 * como consultas, inserções, atualizações e exclusões.
 * Exemplo:
 *    em.getTransaction().begin();
 *    // Realizar operações de banco de dados, e.g., consultar, inserir, etc.
 *    em.getTransaction().commit();
 *    em.close();
 *
 * 4. Fechamento de EntityManagerFactory
 * Ao finalizar a aplicação, é crucial fechar todas as EntityManagerFactory para liberar recursos.
 * Utilize o método closeAll() da EntityManagerFactoryRegistry para fazer isso de maneira centralizada.
 *
 * Exemplo de Código para Fechar EntityManagerFactory:
 *   registry.closeAll();
 *
 *
 * public class Application {
 *
 *    public static void main(String[] args) {
 *        try {
 *            EntityManagerFactoryRegistry registry = EntityManagerFactoryRegistry.getInstance();
 *            EntityManagerFactory emf = registry.getFactory("MyPersistenceUnit");
 *            EntityManager em = emf.createEntityManager();
 *
 *            em.getTransaction().begin();
 *            // Suponha uma operação de consulta ou atualização aqui
 *            em.getTransaction().commit();
 *
 *        } catch (Exception e) {
 *            e.printStackTrace();
 *        } finally {
 *            EntityManagerFactoryRegistry.getInstance().closeAll();
 *        }
 *    }
 * }
 *
 * </pre>
 *
 * @author Marcius
 */
public class EntityManagerFactoryRegistry {

    private static final EntityManagerFactoryRegistry INSTANCE = new EntityManagerFactoryRegistry();
    private final Map<String, EntityManagerFactory> factories = Collections.synchronizedMap(new LinkedHashMap<>());

    private EntityManagerFactoryRegistry() {
        initializeFactories();
    }

    public static EntityManagerFactoryRegistry getInstance() {
        return INSTANCE;
    }

    private void initializeFactories() {
        List<String> persistenceUnits = getListOfPersistenceUnits();
        for (String unitName : persistenceUnits) {
            EntityManagerFactory factory = Persistence.createEntityManagerFactory(unitName);
            factories.put(unitName, factory);
        }
    }

    public EntityManagerFactory getFactory(String unitName) {
        return factories.get(unitName);
    }

    public EntityManagerFactory getFactory() {
        return factories.entrySet().iterator().next().getValue();
    }

    public void closeAll() {
        for (EntityManagerFactory factory : factories.values()) {
            factory.close();
        }
        factories.clear();
    }

    private List<String> getListOfPersistenceUnits() {
        List<String> puList = new LinkedList<>();
        try {

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(ClassLoader.getSystemResourceAsStream("META-INF/persistence.xml"));

            NodeList nodeList = doc.getElementsByTagName("persistence-unit");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element element = (Element) nodeList.item(i);
                String puName = element.getAttribute("name");
                puList.add(puName);
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            throw new RuntimeException(ex);
        }
        return puList;
    }

}
