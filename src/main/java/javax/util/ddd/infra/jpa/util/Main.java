package javax.util.ddd.infra.jpa.util;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

/**
 * Exemplo de uso
 * @author Marcius
 */
public class Main {
    public static void main(String[] args) {
        EntityManagerFactory emf = PersistenceUnitFinder.getEntityManagerFactory();
        EntityManager em = emf.createEntityManager();

        try {
            // Sua lógica aqui
        } finally {
            em.close();
            //PersistenceUnitFinder.closeEntityManagerFactory();
        }
    }
}
