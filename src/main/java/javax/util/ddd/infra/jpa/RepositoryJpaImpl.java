package javax.util.ddd.infra.jpa;

import javax.util.spring.noap.core.PropertyId;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.Parameter;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import javax.util.ddd.domain.IRepository;
import java.util.Optional;
import javax.util.ddd.infra.PageImpl;
import javax.util.ddd.domain.IPage;
import javax.util.ddd.infra.util.Throw;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Marcius Brandão
 */
public class RepositoryJpaImpl implements IRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public void add(Object entity) {
        addAll(new Object[]{entity});
    }

    @Override
    @Transactional
    public void addAll(Object... entitiesOrList) {
        if (entitiesOrList.length == 1 && entitiesOrList[0] instanceof List) {
            List<?> entities = (List<?>) entitiesOrList[0];
            for (Object entity : entities) {
                processEntity(entity);
            }
        } else {
            for (Object entity : entitiesOrList) {
                processEntity(entity);
            }
        }

        getEntityManager().flush();
        /* Envia lote de comandos para o BD */
        getEntityManager().clear();
        /* Limpa o cache primário */
    }
    
    @Override
    @Transactional
//    @Modifying #TODO
    public int add(String jpql, Object... params) {
        Throw.ifIsNull(jpql, "Comando jpql não informado");
        Throw.ifIsTrue(!jpql.toLowerCase().startsWith("insert "), "jpql deve ser um comando INSERT");        
        
        Query query;
        try {
            query = getEntityManager().createQuery(jpql);
        } catch (IllegalArgumentException ex) {
            query = getEntityManager().createNativeQuery(jpql);
        }

        updateQueryParameters(query, params);
        return query.executeUpdate();
    }

    private void processEntity(Object entity) {
        if (PropertyId.getIdProperty(entity).value() == null) {
            getEntityManager().persist(entity);
        } else {
            getEntityManager().merge(entity);
        }
    }

    @Override
    @Transactional
    public void remove(Object entity) {
        removeAll(new Object[]{entity});
    }

    @Override
    @Transactional
    public void removeAll(Object... entities) {
        for (Object entity : entities) {
            Class<?> entityClass = entity.getClass();
            Object oid = PropertyId.getIdProperty(entity).value();
            remove(entityClass, oid);
        }
    }

    @Override
    @Transactional
    public void remove(Class clazz, Object id) {
        Object entity = getEntityManager().find(clazz, id);
        if (entity != null) {
            getEntityManager().remove(entity);
        } else {
            throw new NoSuchElementException(clazz.getSimpleName() + " (" + id + ")");
        }
    }
    
    @Override
    @Transactional
    public int removeAll(String jpql, Object... params) {
        Throw.ifIsNull(jpql, "Comando jpql não informado");
        Throw.ifIsTrue(!jpql.toLowerCase().startsWith("delete "), "jpql deve ser um comando DELETE");
        Throw.ifIsTrue(!jpql.toLowerCase().contains(" where "), "Comando DELETE sem WHERE");
        
        Query query;
        try {
            query = getEntityManager().createQuery(jpql);
        } catch (IllegalArgumentException ex) {
            query = getEntityManager().createNativeQuery(jpql);
        }

        updateQueryParameters(query, params);
        return query.executeUpdate();
    }

    @Override
    @Transactional
    public boolean contains(Class<?> entityClazz, Object id) {
        String entityName = entityClazz.getSimpleName();
        String idFieldName = PropertyId.getIdFieldName(entityClazz);
        Long count = (Long) getEntityManager().createQuery("SELECT COUNT(e) FROM " + entityName + " e WHERE e." + idFieldName + " = :id")
                .setParameter("id", id)
                .getSingleResult();
        return count > 0;
    }

    @Override
    @Transactional
    public boolean contains(Object entity) {
        PropertyId.IdProperty idProperty = PropertyId.getIdProperty(entity);
        return contains(entity.getClass(), idProperty.value());
    }

    @Override
    @Transactional
    public <E> Optional<E> get(Class<E> clazz, Object id) {
        E entity = getEntityManager().find(clazz, id);
        return Optional.ofNullable(entity);
    }

    @Override
    @Transactional
    public <E> Optional<E> get(String namedQuery, Object... parameters) {
        Query query;
        try {
            query = entityManager.createNamedQuery(namedQuery);
        } catch (IllegalArgumentException e) {
            query = entityManager.createQuery(namedQuery);
        }
        updateQueryParameters(query, parameters);

        try {
            E entity = (E) query.getSingleResult();
            return Optional.of(entity);
        } catch (NoResultException ex) {
            return Optional.empty();
        }
    }

    @Override
    @Transactional
    public <E> List<E> getAll(Class<E> clazz) {
        return getAll("SELECT e FROM " + clazz.getCanonicalName() + " e");
    }

    @Override
    @Transactional
    public <E> List<E> getAll(String jpql, Object... params) {
        return (List<E>) subList(jpql, -1, -1, params).getContent();
    }

    @Override
    @Transactional
    public <E> IPage<E> subList(Class<E> entityClass, int pageSize, int pageIndex, Object... params) {
        String jpql = "SELECT e FROM " + entityClass.getCanonicalName() + " e";
        return subList(jpql, pageSize, pageIndex, params);
    }

    @Override
    @Transactional
    public <E> IPage<E> subList(String jpql, int pageSize, int pageIndex, Object... params) {
        Query query = getEntityManager().createQuery(jpql);
        updateQueryParameters(query, params);

        if (pageIndex > 0) {
            query.setFirstResult(pageIndex * pageSize);
        }

        if (pageSize > 0) {
            query.setMaxResults(pageSize);
        }

        List<E> resultList = query.getResultList();

        long totalElements = size(jpql, params);

        // Criar e retornar o objeto Page
        return new PageImpl<>(resultList, pageSize, pageIndex, totalElements);
    }

    @Override
    @Transactional
    public void set(Object entity) {
        setAll(new Object[]{entity});
    }

    @Override
    @Transactional
    public void setAll(Object... entities) {
//        try {
        for (Object o : entities) {
            getEntityManager().merge(o);
        }
        getEntityManager().flush();/*Envia lote de comandos para o BD */
        getEntityManager().clear();/*Limpa o cache primario*/
        //commit = true;
//        } catch (Exception e) {
//            clear();
//            throw new RuntimeException(e);
//        }
    }

    @Override
    @Transactional
    public int setAll(String jpql, Object... params) {
        Throw.ifIsNull(jpql, "Comando jpql não informado");
        Throw.ifIsTrue(!jpql.trim().toLowerCase().startsWith("update "), "jpql deve ser um comando UPDATE");
        Throw.ifIsTrue(!jpql.toLowerCase().contains(" where "), "Comando UPDATE sem WHERE");
        
        Query query;
        try {
            query = getEntityManager().createQuery(jpql);
        } catch (IllegalArgumentException ex) {
            query = getEntityManager().createNativeQuery(jpql);
        }

        updateQueryParameters(query, params);
        return query.executeUpdate();
    }

    @Override
    @Transactional
    public long size(String jpql, Object... params) {
        String jpqlCount = JPQL.transformarParaConsultaDeContagem(jpql);
        Query countQuery = getEntityManager().createQuery(jpqlCount);
        updateQueryParameters(countQuery, params);
        return (long) countQuery.getSingleResult();
    }

    @Override
    @Transactional
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    @Transactional
    public void persistAll() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private static void updateQueryParameters(Query query, Object[] parameters) {
        int position = -1;
        Set<Parameter<?>> qParams = query.getParameters();
        for (Parameter<?> param : qParams) {
            position++;
            if (param.getName() != null) {
                query.setParameter(param.getName(), parameters[position]);
                System.out.print(param.getName() + "(" + position + ")=" + parameters[position]);
            } else if (param.getPosition() != null) {
                query.setParameter(param.getPosition(), parameters[param.getPosition()-1]);
            } else {
                throw new RuntimeException("Não foi possível identificar os parâmetros na query");
            }
        }
    }

    private static final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Override
    public Optional<List<String>> validate(Object entity) {
        List<String> errors = new ArrayList<>();

        Set<ConstraintViolation<Object>> violations = validator.validate(entity);
        if (!violations.isEmpty()) {
            for (ConstraintViolation violation : violations) {
                errors.add(violation.getPropertyPath().toString()
                        .concat(": ")
                        .concat(violation.getMessage()));
            }
            return Optional.of(errors);
        }

        return Optional.empty();
    }

}
