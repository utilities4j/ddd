package javax.util.ddd.infra.jpa;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import java.util.Set;

/**
 *
 * @author Marcius
 */
public class EntityValidator {

    private static final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    private static final Validator validator = factory.getValidator();

    static public void validate(Object object) {
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object);
        for (ConstraintViolation error : constraintViolations) {
            String msgError = error.getMessage();
            System.out.println(msgError);
        }
    }

    static public void handleConstraintViolation(ConstraintViolationException cve) {
        Set<ConstraintViolation<?>> cvs = cve.getConstraintViolations();
        for (ConstraintViolation<?> cv : cvs) {
            System.out.println("------------------------------------------------");
            System.out.println("Violation: " + cv.getMessage());
            System.out.println("Entity: " + cv.getRootBeanClass().getSimpleName());
            // The violation occurred on a leaf bean (embeddable)
            if (cv.getLeafBean() != null && cv.getRootBean() != cv.getLeafBean()) {
                System.out.println("Embeddable: "
                        + cv.getLeafBean().getClass().getSimpleName());
            }
            System.out.println("Attribute: " + cv.getPropertyPath());
            System.out.println("Invalid value: " + cv.getInvalidValue());
        }
    }
}
