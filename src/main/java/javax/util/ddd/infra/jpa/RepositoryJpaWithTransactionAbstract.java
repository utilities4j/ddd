package javax.util.ddd.infra.jpa;

//import jakarta.persistence.EmbeddedId;
//import jakarta.persistence.EntityManager;
//import jakarta.persistence.Id;
//import jakarta.persistence.Parameter;
//import jakarta.persistence.Query;
//import java.lang.reflect.Field;
//import java.util.List;
//import java.util.NoSuchElementException;
//import java.util.Optional;
//import java.util.Set;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.persistence.EmbeddedId;
//import javax.persistence.EntityManager;
//import javax.persistence.Id;
//import javax.persistence.Parameter;
//import javax.persistence.Query;
//import javax.persistence.NoResultException;
import javax.util.ddd.domain.IRepository;
//import javax.util.ddd.infra.jpa.metamodel.EntityInspector;

/**
 *
 * @author Marcius
 * @version 1.0
 */
public abstract class RepositoryJpaWithTransactionAbstract implements IRepository {

//    private static final Logger LOGGER = Logger.getLogger(RepositoryJpaWithTransactionAbstract.class.getName());
//
//    private boolean commit = false;
//    private Boolean isNewTransaction;
//
//    abstract public EntityManager getEntityManager();
//
//    abstract public void setEntityManager(EntityManager entityManager);
//
//    /**
//     * Instancia entityManager a partir da PU default
//     */
//    public RepositoryJpaWithTransactionAbstract() {
//        beginTransaction();
//    }
//
//    private void beginTransaction() {
//        if (isNewTransaction == null) {
//            isNewTransaction = !getEntityManager().getTransaction().isActive();
//        }
//
//        if (!isNewTransaction) {
//            getEntityManager().getTransaction().begin();
//            LOGGER.log(Level.FINE, "Repository beginTransaction...");
//        }
//    }
//
//    /**
//     * Aninhamento de Transações : Fecha a EntityManager se foi a instancia que
//     * criou
//     */
//    private void closeEntityManager() {
//        if (isNewTransaction && getEntityManager() != null) {
//            LOGGER.log(Level.FINE, "Closing EntityManager");
//            getEntityManager().close();
//            setEntityManager(null);
//            LOGGER.log(Level.FINE, "Repository: close()");
//        }
//    }
//
//    @Override
//    public void persistAll() {
//        if (isNewTransaction) {
//            try {
//                getEntityManager().getTransaction().commit();
//                LOGGER.log(Level.FINE, "Entity: commit()");
//            } catch (Exception e) {
//                getEntityManager().getTransaction().rollback();
//                throw new RuntimeException(e);
//            } finally {
//                closeEntityManager();
//            }
//        }
//    }
//
//    @Override
//    public void clear() {
//        //if (isNewTransaction) {
//        try {
//            if (commit) {
//                getEntityManager().getTransaction().rollback();
//                LOGGER.log(Level.FINE, "Repository: rollback()");
//            }
//        } finally {
//            closeEntityManager();
//        }
//        //}
//    }
//
//    @Override
//    public void addAll(Object... objects) {
//        try {
//            for (Object o : objects) {
//                if (getValueId(o) == null) {
//                    getEntityManager().persist(o);
//                } else {
//                    getEntityManager().merge(o);
//                }
//            }
//            getEntityManager().flush();/*Envia lote de comandos para o BD */
//            getEntityManager().clear();/*Limpa o cache primario*/
//            commit = true;
//        } catch (Exception e) {
//            clear();
//            throw new RuntimeException(e);
//        }
//
//        //return this;
//    }
//
//    @Override
//    public void setAll(Object... objects) {
//        try {
//            for (Object o : objects) {
//                getEntityManager().merge(o);
//            }
//            getEntityManager().flush();/*Envia lote de comandos para o BD */
//            getEntityManager().clear();/*Limpa o cache primario*/
//            commit = true;
//        } catch (Exception e) {
//            clear();
//            throw new RuntimeException(e);
//        }
//    }
//
//    @Override
//    public void removeAll(Object... objects) {
//        for (Object o : objects) {
//            o = getEntityManager().merge(o);
//            getEntityManager().remove(o);
//        }
//        commit = true;
//    }
//
//    @Override
//    public boolean contains(Class<?> entityClazz, Object id) {
//        String entityName = entityClazz.getSimpleName();
//        String idFieldName = EntityInspector.getIdFieldName(entityClazz);
//        Long count = (Long) getEntityManager().createQuery("SELECT COUNT(e) FROM " + entityName + " e WHERE e." + idFieldName + " = :id")
//                .setParameter("id", id)
//                .getSingleResult();
//        return count > 0;
//    }
//
//    @Override
//    public boolean contains(Object entity) {
//        return getEntityManager().contains(entity);
//    }
//
//    @Override
//    public <T> Optional<T> get(String namedQuery, Object... parameters) {
//        Query query = getEntityManager().createNamedQuery(namedQuery);
//        updateQueryParameters(query, parameters);
//        T entity = (T) query.getSingleResult();
//        return Optional.ofNullable(entity);
//    }
//
//    @Override
//    public <E> List<E> getAll(String namedQuery, Object... params) {
//        return subList(namedQuery, -1, -1, params);
//    }
//
//    @Override
//    public <E> List<E> subList(String namedQuery, int maxResults, int firstResult, Object... params) {
//        Query query = getEntityManager().createNamedQuery(namedQuery);
//        updateQueryParameters(query, params);
//
//        //jpaQuery.setMaxResults(resultsPerPage);
//        //jpaQuery.setFirstResult((pageNo-1) * resultsPerPage);
//        if (firstResult > 0) {
//            query.setFirstResult(firstResult);
//        }
//
//        if (maxResults > 0) {
//            query.setMaxResults(maxResults);
//        }
//
//        List<E> resultList = query.getResultList();
//        return resultList;
//    }
//
//    @Override
//    public long size(String namedQuery, Object... parameters) {
//        return getAll(namedQuery, -1, -1, parameters).size();
//    }
//
//    private static void updateQueryParameters(Query query, Object[] parameters) {
//        int position = -1;
//        Set<Parameter<?>> qParams = query.getParameters();
//        for (Parameter<?> param : qParams) {
//            position++;
//            if (param.getName() != null) {
//                query.setParameter(param.getName(), parameters[position]);
//                System.out.print(param.getName() + "(" + position + ")=" + parameters[position]);
//            } else if (param.getPosition() != null) {
//                query.setParameter(param.getPosition(), parameters[param.getPosition()]);
//                System.out.print(param.getPosition() + "=" + parameters[param.getPosition()]);
//            } else {
//                throw new RuntimeException("Não foi possível identificar os parâmetros na query");
//            }
//        }
//    }
//
//    private static Object getValueId(final Object object) {
//        Object valueId = null;
//
//        for (Class c = object.getClass(); c != null; c = c.getSuperclass()) {
//            for (final Field field : c.getDeclaredFields()) {
//                if (field.isAnnotationPresent(Id.class) || field.isAnnotationPresent(EmbeddedId.class)) {
//
//                    field.setAccessible(true);
//
//                    try {
//                        valueId = field.get(object);
//                    } catch (IllegalArgumentException ex) {
//                        throw new IllegalArgumentException("O objeto passado como argumento é inválido", ex);
//                    } catch (IllegalAccessException ex) {
//                        throw new RuntimeException("O Id do objeto " + object.toString() + " não pode ser acessado", ex);
//                    }
//                    break;
//                }
//            }
//        }
//        return valueId;
//    }
//
//    @Override
//    public void add(Object entity) {
//        addAll(new Object[]{entity});
//    }
//
//    @Override
//    public void set(Object entity) {
//        setAll(new Object[]{entity});
//    }
//
//    @Override
//    public void remove(Object entity) {
//        removeAll(new Object[]{entity});
//    }
//
//    @Override
//    public void remove(Class entityClazz, Object id) {
//        Object entity = getEntityManager().find(entityClazz, id);
//        if (entity != null) {
//            getEntityManager().remove(entity);
//        } else {
//            throw new NoSuchElementException(entityClazz.getSimpleName() +  " (" + id + ")");
//        }
//    }
//
//    @Override
//    public <E> Optional<E> get(Class<E> clazz, Object id) {
//        E entity = getEntityManager().find(clazz, id);
//        return Optional.ofNullable(entity);
//    }
//
//    @Override
//    public <E> List<E> getAll(Class<E> entityClazz) {
//        return getAll("From " + entityClazz.getCanonicalName());
//    }

}
