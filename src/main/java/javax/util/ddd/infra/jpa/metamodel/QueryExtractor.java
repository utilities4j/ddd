package javax.util.ddd.infra.jpa.metamodel;

import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;


public class QueryExtractor {

    public record NamedQueryInfo(String name, String query) {

    }

    public NamedQueryInfo getNamedQueryInfo(Class<?> entityClass, String queryName) {
        if (entityClass == null) {
            return null;
        }

        // Verifica se há múltiplas named queries na classe
        if (entityClass.isAnnotationPresent(NamedQueries.class)) {
            NamedQueries namedQueries = entityClass.getAnnotation(NamedQueries.class);
            for (NamedQuery namedQuery : namedQueries.value()) {
                if (namedQuery.name().equals(queryName)) {
                    return new NamedQueryInfo(namedQuery.name(), namedQuery.query());
                }
            }
        }

        // Verifica se há uma única named query na classe
        if (entityClass.isAnnotationPresent(NamedQuery.class)) {
            NamedQuery namedQuery = entityClass.getAnnotation(NamedQuery.class);
            if (namedQuery.name().equals(queryName)) {
                return new NamedQueryInfo(namedQuery.name(), namedQuery.query());
            }
        }

        return null; // Não encontrou a named query
    }
}
