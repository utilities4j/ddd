package javax.util.ddd.infra;

import java.io.Serializable;
import java.util.List;
import javax.util.ddd.domain.IPage;

/**
 *
 * @author marcius.brandao
 * @param <E>
 */
public class PageImpl<E> implements Serializable, IPage<E> {
//    public List<T> getContent();

    private final List<E> content;

    private final int totalPages;

    private final long totalElements;

//    public int getNumber();
    private final int number;
//
//    public int getSize();
    private final int size;
//
//    public int getNumberOfElements();
    private final int numberOfElements;
//
//    public boolean hasContent();
    private final boolean hasContent;
//
//    public Sort getSort();
//
//    public boolean isFirst();
    private final boolean first;
//
//    public boolean isLast();
    private final boolean last;
//
//    public boolean hasNext();
    private final boolean hasNext;
//
//    public boolean hasPrevious();
    private final boolean hasPrevious;
    ///////////////

    private final int pageNumber; //Pageable
    private final int pageSize; //Pageable

    public PageImpl(List<E> content, int pageSize, int pageIndex, long totalElements) {
        this.content = content;
        this.hasContent = !content.isEmpty();
        this.numberOfElements = content.size();

        this.number = pageIndex;
        this.pageNumber = pageIndex + 1;
        this.size = pageSize;
        this.pageSize = pageSize;
        this.totalElements = totalElements;
        this.totalPages = (int) Math.ceil((double) totalElements / pageSize);

        this.last = (pageIndex + 1) * pageSize >= totalElements;
        this.first = pageIndex == 0;
        this.hasNext = !last;
        this.hasPrevious = pageIndex > 0;
    }

    /*
     * Interface Page do Spring
     *    public int getTotalPages();
     *    public long getTotalElements();
     */
    @Override
    public int getTotalPages() {
        return totalPages;
    }

    @Override
    public long getTotalElements() {
        return totalElements;
    }

    /*
     * Inteface Slice do Spring
     *
     */
    @Override
    public List<E> getContent() {
        return content;
    }

    @Override
    public boolean isHasContent() {
        return hasContent;
    }

    @Override
    public boolean isFirst() {
        return first;
    }

    @Override
    public boolean isLast() {
        return last;
    }

    @Override
    public boolean isHasNext() {
        return hasNext;
    }

    @Override
    public boolean isHasPrevious() {
        return hasPrevious;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public int getNumberOfElements() {
        return numberOfElements;
    }

    @Override
    public int getNumber() {
        return number;
    }

    /*
     * Interface Pageable
     */
    @Override
    public int getPageSize() {
        return pageSize;
    }

    @Override
    public int getPageNumber() {
        return pageNumber;
    }
}
