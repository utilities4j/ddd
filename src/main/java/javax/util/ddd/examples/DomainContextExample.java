package javax.util.ddd.examples;

import javax.util.ddd.domain.Context;
import javax.util.ddd.domain.IRepository;

/**
 * NOTA:
 * Adicionar a notação @Component do Spring
 */
public class DomainContextExample extends Context {

    /**
     * NOTA:
     * Adicionar a notação @Autowired do Spring
     */
    private IRepository repository;

    @Override
    /**
     * NOTA:
     * Adicionar @jakarta.annotation.PostConstruct
     */
    public void initialize() {
        // Configurações específicas do Spring se necessário
        setRepository(repository);
    }


}
