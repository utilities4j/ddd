package javax.util.ddd.examples;

import java.util.List;
import javax.util.ddd.domain.IRepository;

/**
 *
 * @author Marcius
 */
public class RepositoryJpaStaticImplExample extends RepositoryJpaStandaloneExample implements IRepository {

    protected RepositoryJpaStaticImplExample() {

    }

    public static IRepository getInstance() {
        return new RepositoryJpaStandaloneExample();
    }

    static public void persist(Object object) {
        //getInstance().add(object).persistAll();
    }

    static public void persistAll(Object... objects) {
        //getInstance().add(objects).persistAll();
    }

    static public void delete(Object object) {
        IRepository r = getInstance();
        r.remove(object);
        r.persistAll();
        //getInstance().remove(object).persistAll();
    }

    static public void deleteAll(Object... objects) {
        //getInstance().remove(objects).persistAll();
    }

    static public <T> T queryUnique(String namedQuery, Object... parameters) {
        IRepository r = getInstance();
        T o = (T) r.get(namedQuery, parameters);
        r.clear();
        return o;
    }

    static public <T> List<T> query(String namedQuery, Object... parameters) {
        IRepository r = getInstance();
        List<T> list = r.getAll(namedQuery, parameters);
        r.clear();
        return list;
    }

    static public <T> List<T> subQuery(String namedQuery, int maxResult, int firstResult, Object... params) {
        IRepository r = getInstance();
        List<T> sublist = (List<T>) r.subList(namedQuery, maxResult, firstResult, params).getContent();
        r.clear();
        return sublist;
    }

    static public long queryCount(String namedQuery, Object... parameters) {
        IRepository r = getInstance();
        long size = r.size(namedQuery, parameters);
        r.clear();
        return size;
    }

}
