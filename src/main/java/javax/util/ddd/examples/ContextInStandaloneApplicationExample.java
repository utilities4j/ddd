package javax.util.ddd.examples;

import javax.util.ddd.domain.Context;

public class ContextInStandaloneApplicationExample extends Context {

    @Override
    public void initialize() {
        // Configurações específicas para aplicações standalone
        System.out.println("Standalone ApplicationContext initialized.");
    }


    public static void main(String[] args) {
        // Garante que a classe seja carregada e a instância seja criada
        ContextInStandaloneApplicationExample context1 = new ContextInStandaloneApplicationExample();
        Context.getInstance().setValue("key", "value");
        System.out.println(Context.getInstance().getValue("key"));
        System.out.println(context1.getValue("key"));
    }
}
