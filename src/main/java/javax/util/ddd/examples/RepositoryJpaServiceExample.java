package javax.util.ddd.examples;

import javax.util.ddd.infra.jpa.RepositoryJpaImpl;
import javax.util.ddd.infra.jpa.RepositoryJpaWithTransactionAbstract;

/**
 * Existem 2 tipos de EntityManager : Container-Managed e Application-Managed.
 * Aqui, o contêiner injeta o EntityManager no nosso Repositorio. Ou seja, o
 * container cria o EntityManager do EntityManagerFactory para nós.
 *
 * O contêiner é responsável por iniciar a transação, bem como confirmá-la ou
 * revertê-la. Da mesma forma, o contêiner é responsável por fechar o
 * EntityManager, portanto, é seguro usá-lo sem limpezas manuais. Mesmo se
 * tentarmos fechar um EntityManager gerenciado por contêiner , ele deve lançar
 * um IllegalStateException.
 *
 */
//@org.springframework.stereotype.Repository
public class RepositoryJpaServiceExample extends RepositoryJpaImpl {

    /**
     * Parece que uma instância de EntityManager é compartilhada para todas as
     * operações! No entanto, o contêiner (JakartaEE ou Spring) injeta um proxy
     * especial em vez de um simples EntityManager aqui. E garante que cada
     * EntityManager esteja confinado a um thread.
     *
     * Toda vez que usamos o EntityManager injetado, esse proxy reutilizará o
     * EntityManager existente ou criará um novo. A reutilização geralmente
     * ocorre quando habilitamos algo como Open Session/EntityManager in View.
     */
   
}
