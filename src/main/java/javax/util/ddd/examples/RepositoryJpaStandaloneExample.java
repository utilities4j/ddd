package javax.util.ddd.examples;
//
//import javax.persistence.EntityManager;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import javax.util.ddd.infra.jpa.util.PersistenceUnitFinder;
import javax.util.ddd.infra.jpa.RepositoryJpaImpl;
import javax.util.ddd.infra.jpa.RepositoryJpaWithTransactionAbstract;

//import javax.persistence.EntityManagerFactory;
/**
 * Existem 2 tipos de EntityManager : Container-Managed e Application-Managed. o
 * Aqui, o EntityManager é criado e o ciclo de vida gerenciado pelo aplicativo.
 *
 * Essa classe garante que cada thread (ou seja, cada solicitação) obterá sua
 * própria instância de EntityManager correta chamando getEntityManager()
 *
 * @see
 * https://stackoverflow.com/questions/15071238/entitymanager-threadlocal-pattern-with-jpa-in-jse
 * @author
 */
public class RepositoryJpaStandaloneExample extends RepositoryJpaImpl {

    /**
     * As instâncias EntityManagerFactory (ou SessionFactory do Hibernate) , são
     * thread-safe. Portanto, é completamente seguro em contextos simultâneos
     * escrever:
     */
    private static EntityManagerFactory entityManagerFactory;
    private static final ThreadLocal<EntityManager> threadLocal = new ThreadLocal<>();

    public EntityManagerFactory getEntityManagerFactory() {
        if (entityManagerFactory == null) {
            //  emf = Persistence.createEntityManagerFactory("PU");
            entityManagerFactory = PersistenceUnitFinder.getEntityManagerFactory();
        }
        return entityManagerFactory;
    }

    public void setEntityManagerFactory(EntityManagerFactory emf) {
        entityManagerFactory = emf;
    }

    @Override
    public void setEntityManager(EntityManager entityManager) {
        threadLocal.set(entityManager);
    }

    @Override
    public EntityManager getEntityManager() {
        EntityManager em = threadLocal.get();

        if (em == null) {
            em = getEntityManagerFactory().createEntityManager();
            threadLocal.set(em);
        }
        return em;
    }

    public void closeEntityManager() {
        EntityManager em = threadLocal.get();
        if (em != null) {
            em.close();
            threadLocal.set(null);
        }
    }

}
