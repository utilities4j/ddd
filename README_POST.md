# POST 

## Exemplos de Uso

- **Criando uma nova instância de uma entidade Cliente**

```bash
curl -X POST "http://localhost:8080/domain/cliente/new"
```

**Exemplo de Corpo de Resposta**

```json
{
    "id": null,
    "nome": null,
    "sobreNome": null,
    "limiteDeCredito": 0.0,
    "email": null,
    "idade": 0,
    "endereco": {
        "logradouro": null,
        "cidade": null,
        "estado": null,
        "cep": null
    }
}
```

- **Persisitindo uma entidade Cliente**

```bash
curl -X POST "http://localhost:8080/domain/cliente" \
     -H "Content-Type: application/json" \
  -d '{
        "nome": "Tommy",
        "sobreNome": "Lee Jones",
        "limiteDeCredito": 0.0,
        "email": "tommyleejones@gmail.com",
        "dataNascimento": "1946-09-15",
        "endereco": {
            "logradouro": "Rua dos Artistas",
            "cidade": "Rio de Janeiro",
            "estado": "RJ",
            "cep": "60000000"
        }
    }'
```

****Resposta****

```json
{
  "id": 3
}
```

- **Persistindo Pedido com campos de retorno**

```bash
curl -X POST "http://localhost:8080/domain/pedido?fields=totalItens,valorTotal" \
  -H "Content-Type: application/json" \
  -d '{
    "data": "2024-06-07",
    "cliente": { "id": 1 },
    "entrega": {
      "rua": "Rua Exemplo",
      "numero": "123",
      "cidade": "Cidade Exemplo",
      "estado": "Estado Exemplo",
      "cep": "12345-678"
    },
    "itens": [
      { "id": 1, "nome": "Item 1", "quantidade": 2, "valor" : 1.0 },
      { "id": 2, "nome": "Item 2", "quantidade": 1, "valor" : 2.5 }
    ],
    "vendedores": [
      { "id": 1 },
      { "id": 2 }
    ]
  }'
```

**Resposta**

```json
{
  "id": 1,
  "totalItens" : 3,
  "valorTotal" : 4.5
}
```
