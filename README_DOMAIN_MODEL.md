## Domain Model Example

Nos exemplos acima, utilizamos as entidades `Pedido`, `Item`, `Produto` e `Vendedor`:

```java
import jakarta.persistence.*;

@Entity
public class Pedido {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Cliente cliente;

    @NotNull
    @Temporal(TemporalType.DATE)
    private Date data;

    @Embedded
    private Endereco entrega;

    @Formula("(SELECT SUM(i.quantidade) FROM Item i WHERE i.pedido_id = id)")
    private BigDecimal quantidadeTotal;

    @Formula("(SELECT SUM(i.quantidade * i.valor) FROM Item i WHERE i.pedido_id = id)")
    private BigDecimal valorTotal;

    //Composição : associação onde os objetos "filhos" não faz sentido fora do objeto "pai".
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Item> itens = new ArrayList<>();

    //Agregação : associação onde os objetos "filhos" são independentes do ojeto pai
    @ManyToMany
    private List<Vendedor> vendedores = new ArrayList<>();

    // Getters e setters
}
```

```java
import jakarta.persistence.*;

@Entity
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer quantidade;
    private double valor;

    @ManyToOne
    private Produto produto;

    // Getters e setters
}
```

```java
import jakarta.persistence.*;

@Entity
public class Produto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nome;
    private double preco;

    // Getters e setters
}
```

```java
import jakarta.persistence.*;

@Entity
public class Vendedor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nome;

    // Getters e setters
}
```
